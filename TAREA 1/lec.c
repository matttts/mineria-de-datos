#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define LONG_MAX_LINEA  1024
#define NOM_ARCHIVO  "split-00-matias-mella_tagged"

int main(void)
{
   FILE *entrada;
   char linea[LONG_MAX_LINEA];
   char tipo[4];
   int inf=0, nav=0, res=0;
   float ip=0.0, in=0.0, ir=0.0;

   if ((entrada = fopen(NOM_ARCHIVO, "r")) == NULL){
      perror(NOM_ARCHIVO);
      return EXIT_FAILURE;
   }

   while (fgets(linea, LONG_MAX_LINEA, entrada) != NULL)
   {
	if (linea[0] == 'N' || linea[0] == 'I' || linea[0] == 'R')
	{
		tipo[0] = linea[0];
		tipo[1] = linea[1];
		tipo[2] = linea[2];
		//printf("%s\n", tipo);
		if (tipo[0] == 'N' && tipo[1] == 'A' && tipo[2] == 'V')
			++nav;
		else if (tipo[0] == 'I' && tipo[1] == 'N' && tipo[2] == 'F')
			++inf;
		else
			++res;
	}
	/*else
	{
		printf("SIGUIENTE LINEA: %s\n", linea);
		break;
	}*/
   }

   fclose(entrada);

   ip = inf/3000.0;
   in = nav/3000.0;
   ir = res/3000.0;

   printf("EL TOTAL ES\nINF: %d\nNAV: %d\nRES: %d\n\nTOTAL: %d\n\n", inf, nav, res, inf+nav+res);
   printf("Probabilidades\nINF: %f\nNAV: %f\nRES: %f\n", inf/3000.0, nav/3000.0, res/3000.0);

   return EXIT_SUCCESS;
}

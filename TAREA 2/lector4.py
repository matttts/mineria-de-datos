import MontyTokenizer, MontyTagger, MontyLemmatiser, MontyREChunker, MontyExtractor, MontyNLGenerator, sys

class MontyLingua:
    
    def __init__(self,trace_p=0):
        self.trace_p = trace_p
        self.theMontyTokenizer = MontyTokenizer.MontyTokenizer()
        self.theMontyLemmatiser = MontyLemmatiser.MontyLemmatiser()
        self.theMontyTagger = MontyTagger.MontyTagger(trace_p,self.theMontyLemmatiser)
        self.theMontyChunker = MontyREChunker.MontyREChunker()
        self.theMontyExtractor = MontyExtractor.MontyExtractor()
        self.theMontyNLGenerator = MontyNLGenerator.MontyNLGenerator()
        print '*********************************\n'

#
#  MAIN FUNCTIONS
#
    def generate_summary(self,vsoos):
        return self.theMontyNLGenerator.generate_summary(vsoos)
            
    def generate_sentence(self,vsoo,sentence_type='declaration',tense='past',s_dtnum=('',1),o1_dtnum=('',1),o2_dtnum=('',1),o3_dtnum=('',1)):
        return self.theMontyNLGenerator.generate_sentence(vsoo,sentence_type=sentence_type,tense=tense,s_dtnum=s_dtnum,o1_dtnum=o1_dtnum,o2_dtnum=o2_dtnum,o3_dtnum=o3_dtnum)
        
    def jist_predicates(self,text):
        infos = self.jist(text)
        svoos_list = []
        for info in infos:
            svoos = info['verb_arg_structures_concise']
            svoos_list.append(svoos)
        return svoos_list
    
    def jist(self,text):
        sentences = self.split_sentences(text)
        tokenized = map(self.tokenize,sentences)
        tagged = map(self.tag_tokenized,tokenized)
        chunked = map(self.chunk_tagged,tagged)
        extracted = map(self.extract_info,chunked)
        return extracted

    def pp_info(self,extracted_infos):
        for i in range(len(extracted_infos)):
            keys = extracted_infos[i].keys()
            keys.sort()
            print "\n\n   SENTENCE #%s DIGEST:\n"%str(i+1)
            for key in keys:
                print (key+": ").rjust(22) + str(extracted_infos[i][key])
    
    def split_paragraphs(self,text):
        return self.theMontyTokenizer.split_paragraphs(text)

    def split_sentences(self,text):
        return self.theMontyTokenizer.split_sentences(text)

    def tokenize(self,sentence,expand_contractions_p=1):
        return self.theMontyTokenizer.tokenize(sentence,expand_contractions_p)

    def tag_tokenized(self,tokenized_text):
        return self.theMontyTagger.tag_tokenized(tokenized_text)

    def strip_tags(self,tagged_or_chunked_text):
        toks = tagged_or_chunked_text.split()
        toks = filter(lambda x:'/' in x,toks)
        toks = map(lambda x:x.split('/')[0],toks)
        return ' '.join(toks)

    def parse_pred_arg(self,pp):
        pp.strip
        toks = pp.strip()[1:-1].split()
        args = ' '.join(toks)[1:-1].split('" "')
        return args
    
    def chunk_tagged(self,tagged_text):
        return self.theMontyChunker.Process(tagged_text)
    
    def chunk_lemmatised(self,lemmatised_text):
        return self.theMontyChunker.chunk_multitag(lemmatised_text)

    def lemmatise_tagged(self,tagged_text):
        return self.theMontyLemmatiser.lemmatise_tagged_sentence(tagged_text)

                                   
    def extract_info(self,chunked_text):
    	return self.theMontyExtractor.extract_info(chunked_text,self.theMontyLemmatiser.lemmatise_tagged_sentence)
    
    
file=open('split-00-matias-mella_tagged','r')
linea=file.readline()
m = MontyLingua()

tot=[]
tot_w=[]
tot_f=[]

nav=[]
nav_w=[]
nav_f=[]

res=[]
res_w=[]
res_f=[]

inf=[]
inf_w=[]
inf_f=[]

dict_nav={}
dict_res={}
dict_inf={}

dict_tot={}

punt=['.',',',':',';','-','?']
palabras=['a', 'about', 'above', 'above', 'across', 'after', 'afterwards', 'again', 'against', 'all', 'almost', 'alone', 'along', 'already', 'also','although','always','am','among', 'amongst', 'amoungst', 'amount',  'an', 'and', 'another', 'any','anyhow','anyone','anything','anyway', 'anywhere', 'are', 'around', 'as',  'at', 'back','be','became', 'because','become','becomes', 'becoming', 'been', 'before', 'beforehand', 'behind', 'being', 'below', 'beside', 'besides', 'between', 'beyond', 'bill', 'both', 'bottom','but', 'by', 'call', 'can', 'cannot', 'cant', 'co', 'con', 'could', 'couldnt', 'cry', 'de', 'describe', 'detail', 'do', 'done', 'down', 'due', 'during', 'each', 'eg', 'eight', 'either', 'eleven','else', 'elsewhere', 'empty', 'enough', 'etc', 'even', 'ever', 'every', 'everyone', 'everything', 'everywhere', 'except', 'few', 'fifteen', 'fify', 'fill', 'find', 'fire', 'first', 'five', 'for', 'former', 'formerly', 'forty', 'found', 'four', 'from', 'front', 'full', 'further', 'get', 'give', 'go', 'had', 'has', 'hasnt', 'have', 'he', 'hence', 'her', 'here', 'hereafter', 'hereby', 'herein', 'hereupon', 'hers', 'herself', 'him', 'himself', 'his', 'how', 'however', 'hundred', 'ie', 'if', 'in', 'inc', 'indeed', 'interest', 'into', 'is', 'it', 'its', 'itself', 'keep', 'last', 'latter', 'latterly', 'least', 'less', 'ltd', 'made', 'many', 'may', 'me', 'meanwhile', 'might', 'mill', 'mine', 'more', 'moreover', 'most', 'mostly', 'move', 'much', 'must', 'my', 'myself', 'name', 'namely', 'neither', 'never', 'nevertheless', 'next', 'nine', 'no', 'nobody', 'none', 'noone', 'nor', 'not', 'nothing', 'now', 'nowhere', 'of', 'off', 'often', 'on', 'once', 'one', 'only', 'onto', 'or', 'other', 'others', 'otherwise', 'our', 'ours', 'ourselves', 'out', 'over', 'own','part', 'per', 'perhaps', 'please', 'put', 'rather', 're', 'same', 'see', 'seem', 'seemed', 'seeming', 'seems', 'serious', 'several', 'she', 'should', 'show', 'side', 'since', 'sincere', 'six', 'sixty', 'so', 'some', 'somehow', 'someone', 'something', 'sometime', 'sometimes', 'somewhere', 'still', 'such', 'system', 'take', 'ten', 'than', 'that', 'the', 'their', 'them', 'themselves', 'then', 'thence', 'there', 'thereafter', 'thereby', 'therefore', 'therein', 'thereupon', 'these', 'they', 'thickv', 'thin', 'third', 'this', 'those', 'though', 'three', 'through', 'throughout', 'thru', 'thus', 'to', 'together', 'too', 'top', 'toward', 'towards', 'twelve', 'twenty', 'two', 'un', 'under', 'until', 'up', 'upon', 'us', 'very', 'via', 'was', 'we', 'well', 'were', 'what', 'whatever', 'when', 'whence', 'whenever', 'where', 'whereafter', 'whereas', 'whereby', 'wherein', 'whereupon', 'wherever', 'whether', 'which', 'while', 'whither', 'who', 'whoever', 'whole', 'whom', 'whose', 'why', 'will', 'with', 'within', 'without', 'would', 'yet', 'you', 'your', 'yours', 'yourself', 'yourselves', 'the']

tmp = ""
while linea:
	sp=linea.split('\t')
	sentences = m.split_sentences(sp[1])
        tokenized = map(m.tokenize,sentences)
	for i in tokenized:
		tmp = tmp+" "+i
	if sp[0]=='NAV':
		nav.append(tmp)
	elif sp[0]=='INF':
		inf.append(tmp)
	elif sp[0]=='RES':
		res.append(tmp)
	tot.append(tmp)
	linea=file.readline()
	tmp = ""
#############################################################
for i in res:
	a = i.split()
	for j in a:
		if j not in palabras:
			if j not in punt:
				res_w.append(j)
res_w.sort()

for i in res_w:
	if i not in dict_res:
		dict_res[i]=0

for i in res_w:
	dict_res[i] = dict_res[i]+1

llaves_res=[]
llaves_res = list(dict_res.keys())
llaves_res.sort()

for i in llaves_res:
	par=[]
	par.append(i)
	par.append(dict_res[i])
	res_f.append(par)
################################################################
for i in nav:
	a = i.split()
	for j in a:
		if j not in palabras:
			if j not in punt:
				nav_w.append(j)
nav_w.sort()

for i in nav_w:
	if i not in dict_nav:
		dict_nav[i]=0

for i in nav_w:
	dict_nav[i] = dict_nav[i]+1

llaves_nav=[]
llaves_nav = list(dict_nav.keys())
llaves_nav.sort()

for i in llaves_nav:
	par=[]
	par.append(i)
	par.append(dict_nav[i])
	nav_f.append(par)
##################################################################
for i in inf:
	a = i.split()
	for j in a:
		if j not in palabras:
			if j not in punt:
				inf_w.append(j)
inf_w.sort()

for i in inf_w:
	if i not in dict_inf:
		dict_inf[i]=0

for i in inf_w:
	dict_inf[i] = dict_inf[i]+1

llaves_inf=[]
llaves_inf = list(dict_inf.keys())
llaves_inf.sort()

for i in llaves_inf:
	par=[]
	par.append(i)
	par.append(dict_inf[i])
	inf_f.append(par)
####################################################################
for i in tot:
	a = i.split()
	for j in a:
		if j not in palabras:
			if j not in punt:
				tot_w.append(j)
tot_w.sort()

for i in tot_w:
	if i not in dict_tot:
		dict_tot[i]=0

for i in tot_w:
	dict_tot[i] = dict_tot[i]+1

llaves_tot=[]
llaves_tot = list(dict_tot.keys())
llaves_tot.sort()

for i in llaves_tot:
	par=[]
	par.append(i)
	par.append(dict_tot[i])
	tot_f.append(par)
#############################################
vec_con_res=[]
dict_con={}
for i in res:
	a=i.split()
	for j in a:
		if j not in palabras:
			if j not in punt:
				if j not in dict_con:
					dict_con[j] = 0
	for j in a:
		if j not in palabras:
			if j not in punt:
				dict_con[j] = dict_con[j]+1
	vec_con_res.append(dict_con.items())
	dict_con.clear()
###############################################
vec_con_nav=[]
dict_con.clear()
for i in nav:
	a=i.split()
	for j in a:
		if j not in palabras:
			if j not in punt:
				if j not in dict_con:
					dict_con[j] = 0
	for j in a:
		if j not in palabras:
			if j not in punt:
				dict_con[j] = dict_con[j]+1
	vec_con_nav.append(dict_con.items())
	dict_con.clear()
#################################################
vec_con_inf=[]
dict_con.clear()
for i in inf:
	a=i.split()
	for j in a:
		if j not in palabras:
			if j not in punt:
				if j not in dict_con:
					dict_con[j] = 0
	for j in a:
		if j not in palabras:
			if j not in punt:
				dict_con[j] = dict_con[j]+1
	vec_con_inf.append(dict_con.items())
	dict_con.clear()

dist_i=0.0
dist_r=0.0
dist_n=0.0

d_i=[]
d_n=[]
d_r=[]

d = open("distancias.txt", "w")

for i in dict_res:
	dict_res[i] = dict_res[i]/388.0
for i in dict_nav:
	dict_nav[i] = dict_nav[i]/541.0
for i in dict_inf:
	dict_inf[i] = dict_inf[i]/2071.0

mat_res=[]
tmp=[]
for i in res:
	#tmp.append(i)
	for i in dict_tot:
		tmp.append(0)
	mat_res.append(tmp)
	del tmp
	tmp=[]
	
cont=0
query=0
for i in vec_con_res:
	for j in i:
		#print j[0]
		for k in sorted(dict_tot):
			if k==j[0]:
				break
			cont=cont+1
		#print cont
		mat_res[query][cont]=mat_res[query][cont]+1
		cont=0
		#print mat_res[query][cont]
		#raw_input("CONTINUAR")
	query=query+1
	
mat_inf=[]
del tmp
tmp=[]
for i in inf:
	#tmp.append(i)
	for i in dict_tot:
		tmp.append(0)
	mat_inf.append(tmp)
	del tmp
	tmp=[]
	
cont=0
query=0
for i in vec_con_inf:
	for j in i:
		for k in sorted(dict_tot):
			if k==j[0]:
				break
			cont=cont+1
		mat_inf[query][cont]=mat_inf[query][cont]+1
		cont=0
	query=query+1

mat_nav=[]
del tmp
tmp=[]
for i in nav:
	#tmp.append(i)
	for i in dict_tot:
		tmp.append(0)
	mat_nav.append(tmp)
	del tmp
	tmp=[]
	
cont=0
query=0
for i in vec_con_nav:
	for j in i:
		for k in sorted(dict_tot):
			if k==j[0]:
				break
			cont=cont+1
		mat_nav[query][cont]=mat_nav[query][cont]+1
		cont=0
	query=query+1
	
c_inf=[]
cont=0
#c_inf.append("INF")
for i in sorted(dict_tot):
	if i in dict_inf:
		c_inf.append(str(dict_inf[i]))
	else:
		c_inf.append(0)
c_nav=[]
#c_nav.append("NAV")
for i in sorted(dict_tot):
	if i in dict_nav:
		c_nav.append(str(dict_nav[i]))
	else:
		c_nav.append(0)
c_res=[]
#c_res.append("RES")
for i in sorted(dict_tot):
	if i in dict_res:
		c_res.append(str(dict_res[i]))
	else:
		c_res.append(0)
		
dist_res=[]
del tmp
tmp=[]
cont=0
dist_r=0
dist_n=0
dist_i=0
for i in mat_res:
	for j in i:
		if float(j) > float(c_res[cont]):
			dist_r = dist_r + (float(j)-float(c_res[cont]))
		else:
			dist_r = dist_r + (float(c_res[cont])-float(j))
		if float(j) > float(c_nav[cont]):
			dist_n = dist_n + (float(j)-float(c_nav[cont]))
		else:
			dist_n = dist_n + (float(c_nav[cont])-float(j))
		if float(j) > float(c_inf[cont]):
			dist_i = dist_i + (float(j)-float(c_inf[cont]))
		else:
			dist_i = dist_i + (float(c_inf[cont])-float(j))
		cont=cont+1
	tmp.append(str(dist_i))
	tmp.append(str(dist_n))
	tmp.append(str(dist_r))
	dist_res.append(tmp)
	dist_r=0
	dist_i=0
	dist_n=0
	cont=0
	del tmp
	tmp=[]

for i in dist_res:
	print i

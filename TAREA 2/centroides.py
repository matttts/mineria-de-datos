import MontyTokenizer, MontyTagger, MontyLemmatiser, MontyREChunker, MontyExtractor, MontyNLGenerator, sys, math, MontyLingua
import nltk, re
from collections import Counter
import random
from pylab import plot,show
from numpy import vstack,array
from numpy.random import rand
from scipy.cluster.vq import kmeans,vq
import numpy as np

 
    
file=open("split-00-matias-mella_tagged",'r')
linea=file.readline()
m = MontyLingua.MontyLingua()

a = MontyTagger.MontyTagger(0)

dict_tag={}
dict_tag_common=[]

lem=0
	
tot=[]
tot_w=[]
tot_f=[]

nav=[]
nav_w=[]
nav_f=[]

res=[]
res_w=[]
res_f=[]

inf=[]
inf_w=[]
inf_f=[]

dict_nav={}
dict_res={}
dict_inf={}

dict_tot={}

cant_inf=0.0
cant_nav=0.0
cant_res=0.0
cant_tot=0.0

punt=['.',',',':',';','-','?','&']
palabras=['a', 'about', 'above', 'above', 'across', 'after', 'afterwards', 'again', 'against', 'all', 'almost', 'alone', 'along', 'already', 'also','although','always','am','among', 'amongst', 'amoungst', 'amount',  'an', 'and', 'another', 'any','anyhow','anyone','anything','anyway', 'anywhere', 'are', 'around', 'as',  'at', 'back','be','became', 'because','become','becomes', 'becoming', 'been', 'before', 'beforehand', 'behind', 'being', 'below', 'beside', 'besides', 'between', 'beyond', 'bill', 'both', 'bottom','but', 'by', 'call', 'can', 'cannot', 'cant', 'co', 'con', 'could', 'couldnt', 'cry', 'de', 'describe', 'detail', 'do', 'done', 'down', 'due', 'during', 'each', 'eg', 'eight', 'either', 'eleven','else', 'elsewhere', 'empty', 'enough', 'etc', 'even', 'ever', 'every', 'everyone', 'everything', 'everywhere', 'except', 'few', 'fifteen', 'fify', 'fill', 'find', 'fire', 'first', 'five', 'for', 'former', 'formerly', 'forty', 'found', 'four', 'from', 'front', 'full', 'further', 'get', 'give', 'go', 'had', 'has', 'hasnt', 'have', 'he', 'hence', 'her', 'here', 'hereafter', 'hereby', 'herein', 'hereupon', 'hers', 'herself', 'him', 'himself', 'his', 'how', 'however', 'hundred', 'ie', 'if', 'in', 'inc', 'indeed', 'interest', 'into', 'is', 'it', 'its', 'itself', 'keep', 'last', 'latter', 'latterly', 'least', 'less', 'ltd', 'made', 'many', 'may', 'me', 'meanwhile', 'might', 'mill', 'mine', 'more', 'moreover', 'most', 'mostly', 'move', 'much', 'must', 'my', 'myself', 'name', 'namely', 'neither', 'never', 'nevertheless', 'next', 'nine', 'no', 'nobody', 'none', 'noone', 'nor', 'not', 'nothing', 'now', 'nowhere', 'of', 'off', 'often', 'on', 'once', 'one', 'only', 'onto', 'or', 'other', 'others', 'otherwise', 'our', 'ours', 'ourselves', 'out', 'over', 'own','part', 'per', 'perhaps', 'please', 'put', 'rather', 're', 'same', 'see', 'seem', 'seemed', 'seeming', 'seems', 'serious', 'several', 'she', 'should', 'show', 'side', 'since', 'sincere', 'six', 'sixty', 'so', 'some', 'somehow', 'someone', 'something', 'sometime', 'sometimes', 'somewhere', 'still', 'such', 'system', 'take', 'ten', 'than', 'that', 'the', 'their', 'them', 'themselves', 'then', 'thence', 'there', 'thereafter', 'thereby', 'therefore', 'therein', 'thereupon', 'these', 'they', 'thickv', 'thin', 'third', 'this', 'those', 'though', 'three', 'through', 'throughout', 'thru', 'thus', 'to', 'together', 'too', 'top', 'toward', 'towards', 'twelve', 'twenty', 'two', 'un', 'under', 'until', 'up', 'upon', 'us', 'very', 'via', 'was', 'we', 'well', 'were', 'what', 'whatever', 'when', 'whence', 'whenever', 'where', 'whereafter', 'whereas', 'whereby', 'wherein', 'whereupon', 'wherever', 'whether', 'which', 'while', 'whither', 'who', 'whoever', 'whole', 'whom', 'whose', 'why', 'will', 'with', 'within', 'without', 'would', 'yet', 'you', 'your', 'yours', 'yourself', 'yourselves', 'the']

tmp = ""
string=""
while linea:
	sp=linea.split('\t')
	tag=a.tag(sp[1],0,0)
	#print tag
	v=tag.split()
	for i in v:
		b=i.split("/")
		if b[1] == 'BG' or b[1] == 'JJ' or b[1] == 'NN' or b[1] == 'NS' or b[1] == 'NNS' or b[1] == 'NNP' or b[1] == 'VB' or b[1] == 'J' or b[1] == 'CD' or b[1] == 'S' or b[1] == 'N' or b[1] == 'JJS' or b[1] == 'JJR':
			if b[0] not in punt and b[0] not in palabras:
				string = string + " " + b[0]
	#print string
	#raw_input()
	sentences = m.split_sentences(string)
	tokenized = map(m.tokenize,sentences)
	for i in tokenized:
		tmp = tmp+" "+i
	if sp[0]=='NAV':
		cant_nav=cant_nav+1.0
		nav.append(tmp)
	elif sp[0]=='INF':
		cant_inf=cant_inf+1.0
		inf.append(tmp)
	elif sp[0]=='RES':
		cant_res=cant_res+1.0
		res.append(tmp)
	cant_tot=cant_tot+1.0
	tot.append(tmp)
	linea=file.readline()
	tmp = ""
	string = ""

####################################################################
print 'Generando diccionario de palabras totales\n'
for i in tot:
	a = i.split()
	for j in a:
		if j not in palabras:
			if j not in punt:
				tot_w.append(j)
tot_w.sort()

for i in tot_w:
	if i not in dict_tot:
		dict_tot[i]=0

for i in tot_w:
	dict_tot[i] = dict_tot[i]+1
	
print len(dict_tot)
#############################################################
print 'Generando diccionario de palabras RES'
print 'Generando vector por consultas RES\n'
vec_con_res=[]
dict_con={}
for j in sorted(dict_tot):
		dict_con[j]=0
for i in res:
	a = i.split()
	for j in a:
		if j not in palabras:
			if j not in punt:
				res_w.append(j)
				dict_con[j] += 1
	vec_con_res.append(sorted(list(dict_con.items())))
	for n in sorted(dict_con):
		dict_con[n]=0
res_w.sort()

for i in dict_tot:
	if i not in dict_res:
		dict_res[i]=0

for i in res_w:
	dict_res[i] = dict_res[i]+1
################################################################
print 'Generando diccionario de palabras NAV'
print 'Generando vector por consultas NAV\n'
vec_con_nav=[]
dict_con={}
for j in sorted(dict_tot):
		dict_con[j]=0
for i in nav:
	a = i.split()
	for j in a:
		if j not in palabras:
			if j not in punt:
				nav_w.append(j)
				dict_con[j] += 1
	vec_con_nav.append(sorted(list(dict_con.items())))
	for n in sorted(dict_con):
		dict_con[n]=0
nav_w.sort()

for i in dict_tot:
	if i not in dict_nav:
		dict_nav[i]=0

for i in nav_w:
	dict_nav[i] = dict_nav[i]+1
##################################################################
print 'Generando diccionario de palabras INF'
print 'Generando vector por consultas INF\n'
vec_con_inf=[]
dict_con={}
for j in sorted(dict_tot):
		dict_con[j]=0
for i in inf:
	a = i.split()
	for j in a:
		if j not in palabras:
			if j not in punt:
				inf_w.append(j)
				dict_con[j] += 1
	vec_con_inf.append(sorted(list(dict_con.items())))
	for n in sorted(dict_con):
		dict_con[n]=0
inf_w.sort()

for i in dict_tot:
	if i not in dict_inf:
		dict_inf[i]=0

for i in inf_w:
	dict_inf[i] = dict_inf[i]+1
###################################################################
cent0=0
cent1=0
cent2=0

for i in dict_inf:
	cent0 += (dict_inf[i]/cant_inf)
for i in dict_nav:
	cent1 += (dict_nav[i]/cant_nav)
for i in dict_res:
	cent2 += (dict_res[i]/cant_res)
	
print cent0
print cent1
print cent2

print len(dict_tot)
	
mapeo={}

cont=1

for i in sorted(dict_tot):
	mapeo[i]=cont
	cont += 1
	
tot_mapeado=[]
	
for i in tot:
	pal=i.split()
	temp=[]
	for j in pal:
		temp.append(mapeo[j])
	tot_mapeado.append(temp)
	del temp
	
final=np.zeros((3000,len(mapeo)))

fila=0

for i in tot_mapeado:
	for j in i:
		final[fila][j-1] += 1
	fila += 1
	
data = vstack(final)

centroids,_ = kmeans(data,3)

idx,_ = vq(data,centroids)

a0=0
a1=0
a2=0

for i in idx:
	if i == 0:
		a0 += 1
	elif i == 1:
		a1 += 1
	elif i == 2:
		a2 += 1

print a0
print a1
print a2

cent0=0
cent1=0
cent2=0

for i in centroids[0]:
	cent0 += i
for i in centroids[1]:
	cent1 += i
for i in centroids[2]:
	cent2 += i
	
print cent0
print cent1
print cent2


import MontyTokenizer, MontyTagger, MontyLemmatiser, MontyREChunker, MontyExtractor, MontyNLGenerator, sys, math

class MontyLingua:
    
    def __init__(self,trace_p=0):
        self.trace_p = trace_p
        self.theMontyTokenizer = MontyTokenizer.MontyTokenizer()
        self.theMontyLemmatiser = MontyLemmatiser.MontyLemmatiser()
        self.theMontyTagger = MontyTagger.MontyTagger(trace_p,self.theMontyLemmatiser)
        self.theMontyChunker = MontyREChunker.MontyREChunker()
        self.theMontyExtractor = MontyExtractor.MontyExtractor()
        self.theMontyNLGenerator = MontyNLGenerator.MontyNLGenerator()
        print '*********************************\n'

#
#  MAIN FUNCTIONS
#
    def generate_summary(self,vsoos):
        return self.theMontyNLGenerator.generate_summary(vsoos)
            
    def generate_sentence(self,vsoo,sentence_type='declaration',tense='past',s_dtnum=('',1),o1_dtnum=('',1),o2_dtnum=('',1),o3_dtnum=('',1)):
        return self.theMontyNLGenerator.generate_sentence(vsoo,sentence_type=sentence_type,tense=tense,s_dtnum=s_dtnum,o1_dtnum=o1_dtnum,o2_dtnum=o2_dtnum,o3_dtnum=o3_dtnum)
        
    def jist_predicates(self,text):
        infos = self.jist(text)
        svoos_list = []
        for info in infos:
            svoos = info['verb_arg_structures_concise']
            svoos_list.append(svoos)
        return svoos_list
    
    def jist(self,text):
        sentences = self.split_sentences(text)
        tokenized = map(self.tokenize,sentences)
        tagged = map(self.tag_tokenized,tokenized)
        chunked = map(self.chunk_tagged,tagged)
        extracted = map(self.extract_info,chunked)
        return extracted

    def pp_info(self,extracted_infos):
        for i in range(len(extracted_infos)):
            keys = extracted_infos[i].keys()
            keys.sort()
            print "\n\n   SENTENCE #%s DIGEST:\n"%str(i+1)
            for key in keys:
                print (key+": ").rjust(22) + str(extracted_infos[i][key])
    
    def split_paragraphs(self,text):
        return self.theMontyTokenizer.split_paragraphs(text)

    def split_sentences(self,text):
        return self.theMontyTokenizer.split_sentences(text)

    def tokenize(self,sentence,expand_contractions_p=1):
        return self.theMontyTokenizer.tokenize(sentence,expand_contractions_p)

    def tag_tokenized(self,tokenized_text):
        return self.theMontyTagger.tag_tokenized(tokenized_text)

    def strip_tags(self,tagged_or_chunked_text):
        toks = tagged_or_chunked_text.split()
        toks = filter(lambda x:'/' in x,toks)
        toks = map(lambda x:x.split('/')[0],toks)
        return ' '.join(toks)

    def parse_pred_arg(self,pp):
        pp.strip
        toks = pp.strip()[1:-1].split()
        args = ' '.join(toks)[1:-1].split('" "')
        return args
    
    def chunk_tagged(self,tagged_text):
        return self.theMontyChunker.Process(tagged_text)
    
    def chunk_lemmatised(self,lemmatised_text):
        return self.theMontyChunker.chunk_multitag(lemmatised_text)

    def lemmatise_tagged(self,tagged_text):
        return self.theMontyLemmatiser.lemmatise_tagged_sentence(tagged_text)

                                   
    def extract_info(self,chunked_text):
    	return self.theMontyExtractor.extract_info(chunked_text,self.theMontyLemmatiser.lemmatise_tagged_sentence)
    
    
file=open('split-00-matias-mella_tagged','r')
#file=open('prueba')
linea=file.readline()
m = MontyLingua()

tot=[]
tot_w=[]
tot_f=[]

nav=[]
nav_w=[]
nav_f=[]

res=[]
res_w=[]
res_f=[]

inf=[]
inf_w=[]
inf_f=[]

dict_nav={}
dict_res={}
dict_inf={}

dict_tot={}

cant_inf=0.0
cant_nav=0.0
cant_res=0.0
cant_tot=0.0

punt=['.',',',':',';','-','?','&']
palabras=['a', 'about', 'above', 'above', 'across', 'after', 'afterwards', 'again', 'against', 'all', 'almost', 'alone', 'along', 'already', 'also','although','always','am','among', 'amongst', 'amoungst', 'amount',  'an', 'and', 'another', 'any','anyhow','anyone','anything','anyway', 'anywhere', 'are', 'around', 'as',  'at', 'back','be','became', 'because','become','becomes', 'becoming', 'been', 'before', 'beforehand', 'behind', 'being', 'below', 'beside', 'besides', 'between', 'beyond', 'bill', 'both', 'bottom','but', 'by', 'call', 'can', 'cannot', 'cant', 'co', 'con', 'could', 'couldnt', 'cry', 'de', 'describe', 'detail', 'do', 'done', 'down', 'due', 'during', 'each', 'eg', 'eight', 'either', 'eleven','else', 'elsewhere', 'empty', 'enough', 'etc', 'even', 'ever', 'every', 'everyone', 'everything', 'everywhere', 'except', 'few', 'fifteen', 'fify', 'fill', 'find', 'fire', 'first', 'five', 'for', 'former', 'formerly', 'forty', 'found', 'four', 'from', 'front', 'full', 'further', 'get', 'give', 'go', 'had', 'has', 'hasnt', 'have', 'he', 'hence', 'her', 'here', 'hereafter', 'hereby', 'herein', 'hereupon', 'hers', 'herself', 'him', 'himself', 'his', 'how', 'however', 'hundred', 'ie', 'if', 'in', 'inc', 'indeed', 'interest', 'into', 'is', 'it', 'its', 'itself', 'keep', 'last', 'latter', 'latterly', 'least', 'less', 'ltd', 'made', 'many', 'may', 'me', 'meanwhile', 'might', 'mill', 'mine', 'more', 'moreover', 'most', 'mostly', 'move', 'much', 'must', 'my', 'myself', 'name', 'namely', 'neither', 'never', 'nevertheless', 'next', 'nine', 'no', 'nobody', 'none', 'noone', 'nor', 'not', 'nothing', 'now', 'nowhere', 'of', 'off', 'often', 'on', 'once', 'one', 'only', 'onto', 'or', 'other', 'others', 'otherwise', 'our', 'ours', 'ourselves', 'out', 'over', 'own','part', 'per', 'perhaps', 'please', 'put', 'rather', 're', 'same', 'see', 'seem', 'seemed', 'seeming', 'seems', 'serious', 'several', 'she', 'should', 'show', 'side', 'since', 'sincere', 'six', 'sixty', 'so', 'some', 'somehow', 'someone', 'something', 'sometime', 'sometimes', 'somewhere', 'still', 'such', 'system', 'take', 'ten', 'than', 'that', 'the', 'their', 'them', 'themselves', 'then', 'thence', 'there', 'thereafter', 'thereby', 'therefore', 'therein', 'thereupon', 'these', 'they', 'thickv', 'thin', 'third', 'this', 'those', 'though', 'three', 'through', 'throughout', 'thru', 'thus', 'to', 'together', 'too', 'top', 'toward', 'towards', 'twelve', 'twenty', 'two', 'un', 'under', 'until', 'up', 'upon', 'us', 'very', 'via', 'was', 'we', 'well', 'were', 'what', 'whatever', 'when', 'whence', 'whenever', 'where', 'whereafter', 'whereas', 'whereby', 'wherein', 'whereupon', 'wherever', 'whether', 'which', 'while', 'whither', 'who', 'whoever', 'whole', 'whom', 'whose', 'why', 'will', 'with', 'within', 'without', 'would', 'yet', 'you', 'your', 'yours', 'yourself', 'yourselves', 'the']

tmp = ""
while linea:
	sp=linea.split('\t')
	sentences = m.split_sentences(sp[1])
        tokenized = map(m.tokenize,sentences)
	for i in tokenized:
		tmp = tmp+" "+i
	if sp[0]=='NAV':
		cant_nav=cant_nav+1.0
		nav.append(tmp)
	elif sp[0]=='INF':
		cant_inf=cant_inf+1.0
		inf.append(tmp)
	elif sp[0]=='RES':
		cant_res=cant_res+1.0
		res.append(tmp)
	cant_tot=cant_tot+1.0
	tot.append(tmp)
	linea=file.readline()
	tmp = ""
#############################################################
for i in res:
	a = i.split()
	for j in a:
		if j not in palabras:
			if j not in punt:
				res_w.append(j)
res_w.sort()

for i in res_w:
	if i not in dict_res:
		dict_res[i]=0

for i in res_w:
	dict_res[i] = dict_res[i]+1
################################################################
for i in nav:
	a = i.split()
	for j in a:
		if j not in palabras:
			if j not in punt:
				nav_w.append(j)
nav_w.sort()

for i in nav_w:
	if i not in dict_nav:
		dict_nav[i]=0

for i in nav_w:
	dict_nav[i] = dict_nav[i]+1
##################################################################
for i in inf:
	a = i.split()
	for j in a:
		if j not in palabras:
			if j not in punt:
				inf_w.append(j)
inf_w.sort()

for i in inf_w:
	if i not in dict_inf:
		dict_inf[i]=0

for i in inf_w:
	dict_inf[i] = dict_inf[i]+1
####################################################################
for i in tot:
	a = i.split()
	for j in a:
		if j not in palabras:
			if j not in punt:
				tot_w.append(j)
tot_w.sort()

for i in tot_w:
	if i not in dict_tot:
		dict_tot[i]=0

for i in tot_w:
	dict_tot[i] = dict_tot[i]+1
#############################################
vec_con_res=[]
dict_con={}
for i in res:
	a=i.split()
	for j in a:
		if j not in palabras:
			if j not in punt:
				if j not in dict_con:
					dict_con[j] = 0
	for j in a:
		if j not in palabras:
			if j not in punt:
				dict_con[j] = dict_con[j]+1
	vec_con_res.append(dict(dict_con))
	dict_con.clear()
###############################################
vec_con_nav=[]
dict_con.clear()
for i in nav:
	a=i.split()
	for j in a:
		if j not in palabras:
			if j not in punt:
				if j not in dict_con:
					dict_con[j] = 0
	for j in a:
		if j not in palabras:
			if j not in punt:
				dict_con[j] = dict_con[j]+1
	vec_con_nav.append(dict(dict_con))
	dict_con.clear()
#################################################
vec_con_inf=[]
dict_con.clear()
for i in inf:
	a=i.split()
	for j in a:
		if j not in palabras:
			if j not in punt:
				if j not in dict_con:
					dict_con[j] = 0
	for j in a:
		if j not in palabras:
			if j not in punt:
				dict_con[j] = dict_con[j]+1
	vec_con_inf.append(dict(dict_con))
	dict_con.clear()
################################################
b = open("vector_inf_opt.txt","w")

'''
a=open("dist_opt_man.txt","w")

dist_i=0
dist_n=0
dist_r=0
cont=0
for i in inf:
	for j in sorted(dict_tot):
		if j in dict_inf:
			if j in vec_con_inf[cont]:
				dist_i=dist_i + abs(vec_con_inf[cont][j]-float((dict_inf[j]/cant_inf)))
			else:
				dist_i=dist_i + abs(0-float((dict_inf[j]/cant_inf)))
		else:
			if j in vec_con_inf[cont]:
				dist_i=dist_i + abs(vec_con_inf[cont][j]-0)
			else:
				dist_i=dist_i + abs(0-0)
	for j in sorted(dict_tot):
		if j in dict_nav:
			if j in vec_con_inf[cont]:
				dist_n=dist_n + abs(vec_con_inf[cont][j]-float((dict_nav[j]/cant_nav)))
			else:
				dist_n=dist_n + abs(0-float((dict_nav[j]/cant_nav)))
		else:
			if j in vec_con_inf[cont]:
				dist_n=dist_n + abs(vec_con_inf[cont][j]-0)
			else:
				dist_n=dist_n + abs(0-0)
	for j in sorted(dict_tot):
		if j in dict_res:
			if j in vec_con_inf[cont]:
				dist_r=dist_r + abs(vec_con_inf[cont][j]-float((dict_res[j]/cant_res)))
			else:
				dist_r=dist_r + abs(0-float((dict_res[j]/cant_res)))
		else:
			if j in vec_con_inf[cont]:
				dist_r=dist_r + abs(vec_con_inf[cont][j]-0)
			else:
				dist_r=dist_r + abs(0-0)
	if dist_i < dist_n and dist_i < dist_r:
		mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
		a.write(mm)
	elif dist_n < dist_i and dist_n < dist_r:
		#print '{} {} {} {} NAV'.format(i,dist_i,dist_n,dist_r)
		mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
		a.write(mm)
	elif dist_r < dist_i and dist_r < dist_n:
		#print '{} {} {} {} RES'.format(i,dist_i,dist_n,dist_r)
		mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n"
		a.write(mm)
	else:
		if dist_i==dist_n:
			seed=random.randint(1, 2)
			if seed==1:
				mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
			else:
				mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
		elif dist_i==dist_r:
			seed=random.randint(1, 2)
			if seed==1:
				mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
			else:
				mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n"
		elif dist_n==dist_r:
			seed=random.randint(1, 2)
			if seed==1:
				mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
			else:
				mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n"
		else:
			seed=random.randint(1, 3)
			if seed==1:
				mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
			elif seed==2:
				mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
			else:
				mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n"
		a.write(mm)
	cont=cont+1
	dist_i=0
	dist_n=0
	dist_r=0

dist_i=0
dist_n=0
dist_r=0
cont=0
for i in nav:
	for j in sorted(dict_tot):
		if j in dict_inf:
			if j in vec_con_nav[cont]:
				dist_i=dist_i + abs(vec_con_nav[cont][j]-float((dict_inf[j]/cant_inf)))
			else:
				dist_i=dist_i + abs(0-float((dict_inf[j]/cant_inf)))
		else:
			if j in vec_con_nav[cont]:
				dist_i=dist_i + abs(vec_con_nav[cont][j]-0)
			else:
				dist_i=dist_i + abs(0-0)
	for j in sorted(dict_tot):
		if j in dict_nav:
			if j in vec_con_nav[cont]:
				dist_n=dist_n + abs(vec_con_nav[cont][j]-float((dict_nav[j]/cant_nav)))
			else:
				dist_n=dist_n + abs(0-float((dict_nav[j]/cant_nav)))
		else:
			if j in vec_con_nav[cont]:
				dist_n=dist_n + abs(vec_con_nav[cont][j]-0)
			else:
				dist_n=dist_n + abs(0-0)
	for j in sorted(dict_tot):
		if j in dict_res:
			if j in vec_con_nav[cont]:
				dist_r=dist_r + abs(vec_con_nav[cont][j]-float((dict_res[j]/cant_res)))
			else:
				dist_r=dist_r + abs(0-float((dict_res[j]/cant_res)))
		else:
			if j in vec_con_nav[cont]:
				dist_r=dist_r + abs(vec_con_nav[cont][j]-0)
			else:
				dist_r=dist_r + abs(0-0)
	if dist_i < dist_n and dist_i < dist_r:
		mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
		a.write(mm)
	elif dist_n < dist_i and dist_n < dist_r:
		#print '{} {} {} {} NAV'.format(i,dist_i,dist_n,dist_r)
		mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
		a.write(mm)
	elif dist_r < dist_i and dist_r < dist_n:
		#print '{} {} {} {} RES'.format(i,dist_i,dist_n,dist_r)
		mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n"
		a.write(mm)
	else:
		if dist_i==dist_n:
			seed=random.randint(1, 2)
			if seed==1:
				mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
			else:
				mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
		elif dist_i==dist_r:
			seed=random.randint(1, 2)
			if seed==1:
				mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
			else:
				mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n"
		elif dist_n==dist_r:
			seed=random.randint(1, 2)
			if seed==1:
				mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
			else:
				mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n"
		else:
			seed=random.randint(1, 3)
			if seed==1:
				mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
			elif seed==2:
				mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
			else:
				mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n"
		a.write(mm)
	cont=cont+1
	dist_i=0
	dist_n=0
	dist_r=0

dist_i=0
dist_n=0
dist_r=0
cont=0
for i in res:
	for j in sorted(dict_tot):
		if j in dict_inf:
			if j in vec_con_res[cont]:
				dist_i=dist_i + abs(vec_con_res[cont][j]-float((dict_inf[j]/cant_inf)))
			else:
				dist_i=dist_i + abs(0-float((dict_inf[j]/cant_inf)))
		else:
			if j in vec_con_res[cont]:
				dist_i=dist_i + abs(vec_con_res[cont][j]-0)
			else:
				dist_i=dist_i + abs(0-0)
	for j in sorted(dict_tot):
		if j in dict_nav:
			if j in vec_con_res[cont]:
				dist_n=dist_n + abs(vec_con_res[cont][j]-float((dict_nav[j]/cant_nav)))
			else:
				dist_n=dist_n + abs(0-float((dict_nav[j]/cant_nav)))
		else:
			if j in vec_con_res[cont]:
				dist_n=dist_n + abs(vec_con_res[cont][j]-0)
			else:
				dist_n=dist_n + abs(0-0)
	for j in sorted(dict_tot):
		if j in dict_res:
			if j in vec_con_res[cont]:
				dist_r=dist_r + abs(vec_con_res[cont][j]-float((dict_res[j]/cant_res)))
			else:
				dist_r=dist_r + abs(0-float((dict_res[j]/cant_res)))
		else:
			if j in vec_con_res[cont]:
				dist_r=dist_r + abs(vec_con_res[cont][j]-0)
			else:
				dist_r=dist_r + abs(0-0)
	if dist_i < dist_n and dist_i < dist_r:
		mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
		a.write(mm)
	elif dist_n < dist_i and dist_n < dist_r:
		#print '{} {} {} {} NAV'.format(i,dist_i,dist_n,dist_r)
		mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
		a.write(mm)
	elif dist_r < dist_i and dist_r < dist_n:
		#print '{} {} {} {} RES'.format(i,dist_i,dist_n,dist_r)
		mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n"
		a.write(mm)
	else:
		if dist_i==dist_n:
			seed=random.randint(1, 2)
			if seed==1:
				mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
			else:
				mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
		elif dist_i==dist_r:
			seed=random.randint(1, 2)
			if seed==1:
				mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
			else:
				mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n"
		elif dist_n==dist_r:
			seed=random.randint(1, 2)
			if seed==1:
				mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
			else:
				mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n"
		else:
			seed=random.randint(1, 3)
			if seed==1:
				mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
			elif seed==2:
				mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
			else:
				mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n"
		a.write(mm)
	cont=cont+1
	dist_i=0
	dist_n=0
	dist_r=08

a.close()
'''

a=open("dist_opt_sqr.txt","w")

dist_i=0
dist_n=0
dist_r=0
cont=0
for i in inf:
	#print i
	for j in sorted(dict_tot):
		#print j
		if j in dict_inf:
			if j in vec_con_inf[cont]:
				lala= '{}-{}/{}'.format(vec_con_inf[cont][j],dict_inf[j],cant_inf)
				b.write(lala)
				b.write("\n")
				lala= '{}-{}'.format(math.sqrt(vec_con_inf[cont][j]),math.sqrt(float((dict_inf[j]/cant_inf))))
				b.write(lala)
				b.write("\n")
				lala= '{}'.format(pow(math.sqrt(vec_con_inf[cont][j])-math.sqrt(float((dict_inf[j]/cant_inf))),2))
				b.write(lala)
				b.write("\n")
				dist_i=dist_i + pow((math.sqrt(vec_con_inf[cont][j])-math.sqrt(float((dict_inf[j]/cant_inf)))),2)
			else:
				lala= '{}-{}/{}'.format(0,dict_inf[j],cant_inf)
				b.write(lala)
				b.write("\n")
				lala= '{}-{}'.format(0,math.sqrt(float((dict_inf[j]/cant_inf))))
				b.write(lala)
				b.write("\n")
				lala= '{}'.format(0-math.sqrt(float((dict_inf[j]/cant_inf))),2)
				b.write(lala)
				b.write("\n")
				dist_i=dist_i + pow(0-math.sqrt(float((dict_inf[j]/cant_inf))),2)
		else:
			if j in vec_con_inf[cont]:
				lala= '{}-{}/{}'.format(vec_con_inf[cont][j],0,cant_inf)
				b.write(lala)
				b.write("\n")
				lala= '{}-{}'.format(math.sqrt(vec_con_inf[cont][j]),0)
				b.write(lala)
				b.write("\n")
				lala= '{}'.format(pow(math.sqrt(vec_con_inf[cont][j])-0,2))
				b.write(lala)
				b.write("\n")
				dist_i=dist_i + pow(math.sqrt(vec_con_inf[cont][j])-0,2)
			else:
				lala= '0-0'
				dist_i=dist_i + abs(0-0)
		b.write(lala)
		b.write("\n")
	for j in sorted(dict_tot):
		if j in dict_nav:
			if j in vec_con_inf[cont]:
				dist_n=dist_n + pow(math.sqrt(vec_con_inf[cont][j])-math.sqrt(float((dict_nav[j]/cant_nav))),2)
			else:
				dist_n=dist_n + pow(0-math.sqrt(float((dict_nav[j]/cant_nav))),2)
		else:
			if j in vec_con_inf[cont]:
				dist_n=dist_n + pow(math.sqrt(vec_con_inf[cont][j])-0,2)
			else:
				dist_n=dist_n + abs(0-0)
	for j in sorted(dict_tot):
		if j in dict_res:
			if j in vec_con_inf[cont]:
				dist_r=dist_r + pow(math.sqrt(vec_con_inf[cont][j])-math.sqrt(float((dict_res[j]/cant_res))),2)
			else:
				dist_r=dist_r + pow(0-math.sqrt(float((dict_res[j]/cant_res))),2)
		else:
			if j in vec_con_inf[cont]:
				dist_r=dist_r + pow(math.sqrt(vec_con_inf[cont][j])-0,2)
			else:
				dist_r=dist_r + abs(0-0)
	if dist_i < dist_n and dist_i < dist_r:
		mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
		a.write(mm)
	elif dist_n < dist_i and dist_n < dist_r:
		#print '{} {} {} {} NAV'.format(i,dist_i,dist_n,dist_r)
		mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
		a.write(mm)
	elif dist_r < dist_i and dist_r < dist_n:
		#print '{} {} {} {} RES'.format(i,dist_i,dist_n,dist_r)
		mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n"
		a.write(mm)
	else:
		if dist_i==dist_n:
			seed=random.randint(1, 2)
			if seed==1:
				mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
			else:
				mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
		elif dist_i==dist_r:
			seed=random.randint(1, 2)
			if seed==1:
				mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
			else:
				mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n"
		elif dist_n==dist_r:
			seed=random.randint(1, 2)
			if seed==1:
				mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
			else:
				mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n"
		else:
			seed=random.randint(1, 3)
			if seed==1:
				mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
			elif seed==2:
				mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
			else:
				mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n"
		a.write(mm)
	cont=cont+1
	dist_i=0
	dist_n=0
	dist_r=0

dist_i=0
dist_n=0
dist_r=0
cont=0
for i in nav:
	for j in sorted(dict_tot):
		if j in dict_inf:
			if j in vec_con_nav[cont]:
				#print '{}-{}'.format(j,j)
				dist_i=dist_i + pow(math.sqrt(vec_con_nav[cont][j])-math.sqrt(float((dict_inf[j]/cant_inf))),2)
			else:
				#print '{}-{}'.format(0,j)
				dist_i=dist_i + pow(0-math.sqrt(float((dict_inf[j]/cant_inf))),2)
		else:
			if j in vec_con_nav[cont]:
				#print '{}-{}'.format(j,0)
				dist_i=dist_i + pow(math.sqrt(vec_con_nav[cont][j])-0,2)
			else:
				#print '0-0'
				dist_i=dist_i + abs(0-0)
	for j in sorted(dict_tot):
		if j in dict_nav:
			if j in vec_con_nav[cont]:
				dist_n=dist_n + pow(math.sqrt(vec_con_nav[cont][j])-math.sqrt(float((dict_nav[j]/cant_nav))),2)
			else:
				dist_n=dist_n + pow(0-math.sqrt(float((dict_nav[j]/cant_nav))),2)
		else:
			if j in vec_con_nav[cont]:
				dist_n=dist_n + pow(math.sqrt(vec_con_nav[cont][j])-0,2)
			else:
				dist_n=dist_n + abs(0-0)
	for j in sorted(dict_tot):
		if j in dict_res:
			if j in vec_con_nav[cont]:
				dist_r=dist_r + pow(math.sqrt(vec_con_nav[cont][j])-math.sqrt(float((dict_res[j]/cant_res))),2)
			else:
				dist_r=dist_r + pow(0-math.sqrt(float((dict_res[j]/cant_res))),2)
		else:
			if j in vec_con_nav[cont]:
				dist_r=dist_r + pow(math.sqrt(vec_con_nav[cont][j])-0,2)
			else:
				dist_r=dist_r + abs(0-0)
	if dist_i < dist_n and dist_i < dist_r:
		mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
		a.write(mm)
	elif dist_n < dist_i and dist_n < dist_r:
		#print '{} {} {} {} NAV'.format(i,dist_i,dist_n,dist_r)
		mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
		a.write(mm)
	elif dist_r < dist_i and dist_r < dist_n:
		#print '{} {} {} {} RES'.format(i,dist_i,dist_n,dist_r)
		mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n"
		a.write(mm)
	else:
		if dist_i==dist_n:
			seed=random.randint(1, 2)
			if seed==1:
				mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
			else:
				mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
		elif dist_i==dist_r:
			seed=random.randint(1, 2)
			if seed==1:
				mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
			else:
				mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n"
		elif dist_n==dist_r:
			seed=random.randint(1, 2)
			if seed==1:
				mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
			else:
				mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n"
		else:
			seed=random.randint(1, 3)
			if seed==1:
				mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
			elif seed==2:
				mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
			else:
				mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n"
		a.write(mm)
	cont=cont+1
	dist_i=0
	dist_n=0
	dist_r=0

dist_i=0
dist_n=0
dist_r=0
cont=0
for i in res:
	for j in sorted(dict_tot):
		if j in dict_inf:
			if j in vec_con_res[cont]:
				dist_i=dist_i + pow(math.sqrt(vec_con_res[cont][j])-math.sqrt(float((dict_inf[j]/cant_inf))),2)
			else:
				dist_i=dist_i + pow(0-math.sqrt(float((dict_inf[j]/cant_inf))),2)
		else:
			if j in vec_con_res[cont]:
				dist_i=dist_i + pow(math.sqrt(vec_con_res[cont][j])-0,2)
			else:
				dist_i=dist_i + abs(0-0)
	for j in sorted(dict_tot):
		if j in dict_nav:
			if j in vec_con_res[cont]:
				dist_n=dist_n + pow(math.sqrt(vec_con_res[cont][j])-math.sqrt(float((dict_nav[j]/cant_nav))),2)
			else:
				dist_n=dist_n + pow(0-math.sqrt(float((dict_nav[j]/cant_nav))),2)
		else:
			if j in vec_con_res[cont]:
				dist_n=dist_n + pow(math.sqrt(vec_con_res[cont][j])-0,2)
			else:
				dist_n=dist_n + abs(0-0)
	for j in sorted(dict_tot):
		if j in dict_res:
			if j in vec_con_res[cont]:
				dist_r=dist_r + pow(math.sqrt(vec_con_res[cont][j])-math.sqrt(float((dict_res[j]/cant_res))),2)
			else:
				dist_r=dist_r + pow(0-math.sqrt(float((dict_res[j]/cant_res))),2)
		else:
			if j in vec_con_res[cont]:
				dist_r=dist_r + pow(math.sqrt(vec_con_res[cont][j])-0,2)
			else:
				dist_r=dist_r + abs(0-0)
	if dist_i < dist_n and dist_i < dist_r:
		mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
		a.write(mm)
	elif dist_n < dist_i and dist_n < dist_r:
		#print '{} {} {} {} NAV'.format(i,dist_i,dist_n,dist_r)
		mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
		a.write(mm)
	elif dist_r < dist_i and dist_r < dist_n:
		#print '{} {} {} {} RES'.format(i,dist_i,dist_n,dist_r)
		mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n"
		a.write(mm)
	else:
		if dist_i==dist_n:
			seed=random.randint(1, 2)
			if seed==1:
				mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
			else:
				mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
		elif dist_i==dist_r:
			seed=random.randint(1, 2)
			if seed==1:
				mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
			else:
				mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n"
		elif dist_n==dist_r:
			seed=random.randint(1, 2)
			if seed==1:
				mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
			else:
				mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n"
		else:
			seed=random.randint(1, 3)
			if seed==1:
				mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
			elif seed==2:
				mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
			else:
				mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n"
		a.write(mm)
	cont=cont+1
	dist_i=0
	dist_n=0
	dist_r=0

a.close()


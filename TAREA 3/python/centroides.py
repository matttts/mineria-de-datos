import MontyTokenizer, MontyTagger, MontyLemmatiser, MontyREChunker, MontyExtractor, MontyNLGenerator, sys, math, MontyLingua
import nltk, re
from collections import Counter
import random
from pylab import plot,show
from numpy import vstack,array
from numpy.random import rand
from scipy.cluster.vq import kmeans,vq,kmeans2
import numpy as np

 
    
file=open("split-00-matias-mella_tagged",'r')
linea=file.readline()
m = MontyLingua.MontyLingua()

a = MontyTagger.MontyTagger(0)

dict_tag={}
dict_tag_common=[]

tags=[]

lem=0
	
tot=[]
tot_w=[]
tot_f=[]

nav=[]
nav_w=[]
nav_f=[]

res=[]
res_w=[]
res_f=[]

inf=[]
inf_w=[]
inf_f=[]

dict_nav={}
dict_res={}
dict_inf={}

dict_tot={}

cant_inf=0.0
cant_nav=0.0
cant_res=0.0
cant_tot=0.0

punt=['.',',',':',';','-','?','&']
palabras=['a', 'about', 'above', 'above', 'across', 'after', 'afterwards', 'again', 'against', 'all', 'almost', 'alone', 'along', 'already', 'also','although','always','am','among', 'amongst', 'amoungst', 'amount',  'an', 'and', 'another', 'any','anyhow','anyone','anything','anyway', 'anywhere', 'are', 'around', 'as',  'at', 'back','be','became', 'because','become','becomes', 'becoming', 'been', 'before', 'beforehand', 'behind', 'being', 'below', 'beside', 'besides', 'between', 'beyond', 'bill', 'both', 'bottom','but', 'by', 'call', 'can', 'cannot', 'cant', 'co', 'con', 'could', 'couldnt', 'cry', 'de', 'describe', 'detail', 'do', 'done', 'down', 'due', 'during', 'each', 'eg', 'eight', 'either', 'eleven','else', 'elsewhere', 'empty', 'enough', 'etc', 'even', 'ever', 'every', 'everyone', 'everything', 'everywhere', 'except', 'few', 'fifteen', 'fify', 'fill', 'find', 'fire', 'first', 'five', 'for', 'former', 'formerly', 'forty', 'found', 'four', 'from', 'front', 'full', 'further', 'get', 'give', 'go', 'had', 'has', 'hasnt', 'have', 'he', 'hence', 'her', 'here', 'hereafter', 'hereby', 'herein', 'hereupon', 'hers', 'herself', 'him', 'himself', 'his', 'how', 'however', 'hundred', 'ie', 'if', 'in', 'inc', 'indeed', 'interest', 'into', 'is', 'it', 'its', 'itself', 'keep', 'last', 'latter', 'latterly', 'least', 'less', 'ltd', 'made', 'many', 'may', 'me', 'meanwhile', 'might', 'mill', 'mine', 'more', 'moreover', 'most', 'mostly', 'move', 'much', 'must', 'my', 'myself', 'name', 'namely', 'neither', 'never', 'nevertheless', 'next', 'nine', 'no', 'nobody', 'none', 'noone', 'nor', 'not', 'nothing', 'now', 'nowhere', 'of', 'off', 'often', 'on', 'once', 'one', 'only', 'onto', 'or', 'other', 'others', 'otherwise', 'our', 'ours', 'ourselves', 'out', 'over', 'own','part', 'per', 'perhaps', 'please', 'put', 'rather', 're', 'same', 'see', 'seem', 'seemed', 'seeming', 'seems', 'serious', 'several', 'she', 'should', 'show', 'side', 'since', 'sincere', 'six', 'sixty', 'so', 'some', 'somehow', 'someone', 'something', 'sometime', 'sometimes', 'somewhere', 'still', 'such', 'system', 'take', 'ten', 'than', 'that', 'the', 'their', 'them', 'themselves', 'then', 'thence', 'there', 'thereafter', 'thereby', 'therefore', 'therein', 'thereupon', 'these', 'they', 'thickv', 'thin', 'third', 'this', 'those', 'though', 'three', 'through', 'throughout', 'thru', 'thus', 'to', 'together', 'too', 'top', 'toward', 'towards', 'twelve', 'twenty', 'two', 'un', 'under', 'until', 'up', 'upon', 'us', 'very', 'via', 'was', 'we', 'well', 'were', 'what', 'whatever', 'when', 'whence', 'whenever', 'where', 'whereafter', 'whereas', 'whereby', 'wherein', 'whereupon', 'wherever', 'whether', 'which', 'while', 'whither', 'who', 'whoever', 'whole', 'whom', 'whose', 'why', 'will', 'with', 'within', 'without', 'would', 'yet', 'you', 'your', 'yours', 'yourself', 'yourselves', 'the']

tmp = ""
string=""
while linea:
	sp=linea.split('\t')
	tag=a.tag(sp[1],0,0)
	#print tag
	v=tag.split()
	for i in v:
		b=i.split("/")
		if b[1] == 'BG' or b[1] == 'JJ' or b[1] == 'NN' or b[1] == 'NS' or b[1] == 'NNS' or b[1] == 'NNP' or b[1] == 'VB' or b[1] == 'J' or b[1] == 'CD' or b[1] == 'S' or b[1] == 'N' or b[1] == 'JJS' or b[1] == 'JJR':
			if b[0] not in punt and b[0] not in palabras:
				string = string + " " + b[0]
	#print string
	#raw_input()
	sentences = m.split_sentences(string)
	tokenized = map(m.tokenize,sentences)
	for i in tokenized:
		tmp = tmp+" "+i
	if sp[0]=='NAV':
		tags.append("NAV")
		cant_nav=cant_nav+1.0
		nav.append(tmp)
	elif sp[0]=='INF':
		tags.append("INF")
		cant_inf=cant_inf+1.0
		inf.append(tmp)
	elif sp[0]=='RES':
		tags.append("RES")
		cant_res=cant_res+1.0
		res.append(tmp)
	cant_tot=cant_tot+1.0
	tot.append(tmp)
	linea=file.readline()
	tmp = ""
	string = ""

####################################################################
print 'Generando diccionario de palabras totales\n'
for i in tot:
	a = i.split()
	for j in a:
		if j not in palabras:
			if j not in punt:
				tot_w.append(j)
tot_w.sort()

for i in tot_w:
	if i not in dict_tot:
		dict_tot[i]=0

for i in tot_w:
	dict_tot[i] = dict_tot[i]+1
	
print len(dict_tot)
######################################################################	
mapeo={}

cont=1

for i in sorted(dict_tot):
	mapeo[i]=cont
	cont += 1
	
tot_mapeado=[]
	
for i in tot:
	pal=i.split()
	temp=[]
	for j in pal:
		temp.append(mapeo[j])
	tot_mapeado.append(temp)
	del temp
	
######################################################################
print "Generando matriz de puntos para cada query"	
print len(tot)
final=np.zeros((3000,len(mapeo)))

fila=0

for i in tot_mapeado:
	for j in i:
		final[fila][j-1] += 1
	fila += 1
	
data = vstack(final)


centroids,_ = kmeans(data,3)
#centroids,_ = kmeans(data,3,40)

idx,_ = vq(data,centroids)

a0=0
a1=0
a2=0

for i in idx:
	if i == 0:
		a0 += 1
	elif i == 1:
		a1 += 1
	elif i == 2:
		a2 += 1

print a0
print a1
print a2

cent0=0
cent1=0
cent2=0

for i in centroids[0]:
	cent0 += i
for i in centroids[1]:
	cent1 += i
for i in centroids[2]:
	cent2 += i
	
print cent0
print cent1
print cent2

tag_kmeans=[]

inf=max(a0,a1,a2)
res=min(a0,a1,a2)

if a0==inf:
	t_inf=0
elif a1==inf:
	t_inf=1
elif a2==inf:
	t_inf=2
	
if a0==res:
	t_res=0
elif a1==res:
	t_res=1
elif a2==res:
	t_res=2
	
t_nav=3-(t_inf+t_res)

#print 'INF {0}, NAV {1}, RES {2}.'.format(t_inf, t_nav, t_res)

for i in idx:
	if i==t_inf:
		tag_kmeans.append("INF")
	elif i==t_nav:
		tag_kmeans.append("NAV")
	elif i==t_res:
		tag_kmeans.append("RES")

resultado=np.zeros((3,3),dtype=np.int)

'''for i in range(0,len(tags)):
	print tags[i]
	print tag_kmeans[i]'''
for i in range(0,len(tags)):
	if tags[i] == "INF":
		if tag_kmeans[i] == "INF":
			resultado[0][0] += 1
		elif tag_kmeans[i] == "NAV":
			resultado[0][1] += 1
		elif tag_kmeans[i] == "RES":
			resultado[0][2] += 1
	elif tags[i] == "NAV":
		if tag_kmeans[i] == "INF":
			resultado[1][0] += 1
		elif tag_kmeans[i] == "NAV":
			resultado[1][1] += 1
		elif tag_kmeans[i] == "RES":
			resultado[1][2] += 1
	elif tags[i] == "RES":
		if tag_kmeans[i] == "INF":
			resultado[2][0] += 1
		elif tag_kmeans[i] == "NAV":
			resultado[2][1] += 1
		elif tag_kmeans[i] == "RES":
			resultado[2][2] += 1
			
for i in resultado:
	print i
print ''

acc_m=((resultado[0][0]+resultado[1][1]+resultado[2][2])/(cant_inf+cant_nav+cant_res))
if resultado[0][0] > 0 and resultado[0][1] > 0 and resultado[0][2] > 0:
	rec_i_m=resultado[0][0]/float(resultado[0][0]+resultado[0][1]+resultado[0][2])
else:
	rec_i_m=0
if resultado[1][0] > 0 and resultado[1][1] > 0 and resultado[1][2] > 0:
	rec_n_m=resultado[1][1]/float(resultado[1][0]+resultado[1][1]+resultado[1][2])
else:
	rec_n_m=0
if resultado[2][0] > 0 and resultado[2][1] > 0 and resultado[2][2] > 0:
	rec_r_m=resultado[2][2]/float(resultado[2][0]+resultado[2][1]+resultado[2][2])
else:
	rec_r_m=0
if resultado[0][0] > 0 and resultado[1][0] > 0 and resultado[2][0] > 0:
	pre_i_m=resultado[0][0]/float(resultado[0][0]+resultado[1][0]+resultado[2][0])
else:
	pre_i_m=0
if resultado[0][1] > 0 and resultado[1][1] > 0 and resultado[2][1] > 0:
	pre_n_m=resultado[1][1]/float(resultado[0][1]+resultado[1][1]+resultado[2][1])
else:
	pre_n_m=0
if resultado[0][2] > 0 and resultado[1][2] > 0 and resultado[2][2] > 0:
	pre_r_m=resultado[2][2]/float(resultado[0][2]+resultado[1][2]+resultado[2][2])
else:
	pre_r_m=0
print 'Accuracy: \n\t{}%'.format(((resultado[0][0]+resultado[1][1]+resultado[2][2])/(cant_inf+cant_nav+cant_res))*100.0)
print 'Recall: \n\tINF {}%\n\tNAV {}%\n\tRES {}%'.format(rec_i_m*100.0,rec_n_m*100.0,rec_r_m*100.0)
print 'Precision: \n\tINF {}%\n\tNAV {}%\n\tRES {}%'.format(pre_i_m*100.0,pre_n_m*100.0,pre_r_m*100.0)
if pre_i_m > 0 and rec_i_m > 0:
	f_i=2*((pre_i_m*rec_i_m)/(pre_i_m+rec_i_m))
else:
	f_i=0
if pre_n_m > 0 and rec_n_m > 0:
	f_n=2*((pre_n_m*rec_n_m)/(pre_n_m+rec_n_m))
else:
	f_n=0
if pre_r_m > 0 and rec_r_m > 0:
	f_r=2*((pre_r_m*rec_r_m)/(pre_r_m+rec_r_m))
else:
	f_r=0
print 'F1 Score: \n\tINF {}\n\tNAV {}\n\tRES {}'.format(f_i,f_n,f_r)


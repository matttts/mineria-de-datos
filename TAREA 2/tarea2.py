import MontyTokenizer, MontyTagger, MontyLemmatiser, MontyREChunker, MontyExtractor, MontyNLGenerator, sys, math
import nltk, re
from collections import Counter
import random


class MontyLingua:
    
    def __init__(self,trace_p=0):
        self.trace_p = trace_p
        self.theMontyTokenizer = MontyTokenizer.MontyTokenizer()
        self.theMontyLemmatiser = MontyLemmatiser.MontyLemmatiser()
        self.theMontyTagger = MontyTagger.MontyTagger(trace_p,self.theMontyLemmatiser)
        self.theMontyChunker = MontyREChunker.MontyREChunker()
        self.theMontyExtractor = MontyExtractor.MontyExtractor()
        self.theMontyNLGenerator = MontyNLGenerator.MontyNLGenerator()
        print '*********************************\n'

#
#  MAIN FUNCTIONS
#
    def generate_summary(self,vsoos):
        return self.theMontyNLGenerator.generate_summary(vsoos)
            
    def generate_sentence(self,vsoo,sentence_type='declaration',tense='past',s_dtnum=('',1),o1_dtnum=('',1),o2_dtnum=('',1),o3_dtnum=('',1)):
        return self.theMontyNLGenerator.generate_sentence(vsoo,sentence_type=sentence_type,tense=tense,s_dtnum=s_dtnum,o1_dtnum=o1_dtnum,o2_dtnum=o2_dtnum,o3_dtnum=o3_dtnum)
        
    def jist_predicates(self,text):
        infos = self.jist(text)
        svoos_list = []
        for info in infos:
            svoos = info['verb_arg_structures_concise']
            svoos_list.append(svoos)
        return svoos_list
    
    def jist(self,text):
        sentences = self.split_sentences(text)
        tokenized = map(self.tokenize,sentences)
        tagged = map(self.tag_tokenized,tokenized)
        chunked = map(self.chunk_tagged,tagged)
        extracted = map(self.extract_info,chunked)
        return extracted

    def pp_info(self,extracted_infos):
        for i in range(len(extracted_infos)):
            keys = extracted_infos[i].keys()
            keys.sort()
            print "\n\n   SENTENCE #%s DIGEST:\n"%str(i+1)
            for key in keys:
                print (key+": ").rjust(22) + str(extracted_infos[i][key])
    
    def split_paragraphs(self,text):
        return self.theMontyTokenizer.split_paragraphs(text)

    def split_sentences(self,text):
        return self.theMontyTokenizer.split_sentences(text)

    def tokenize(self,sentence,expand_contractions_p=1):
        return self.theMontyTokenizer.tokenize(sentence,expand_contractions_p)

    def tag_tokenized(self,tokenized_text):
        return self.theMontyTagger.tag_tokenized(tokenized_text)

    def strip_tags(self,tagged_or_chunked_text):
        toks = tagged_or_chunked_text.split()
        toks = filter(lambda x:'/' in x,toks)
        toks = map(lambda x:x.split('/')[0],toks)
        return ' '.join(toks)

    def parse_pred_arg(self,pp):
        pp.strip
        toks = pp.strip()[1:-1].split()
        args = ' '.join(toks)[1:-1].split('" "')
        return args
    
    def chunk_tagged(self,tagged_text):
        return self.theMontyChunker.Process(tagged_text)
    
    def chunk_lemmatised(self,lemmatised_text):
        return self.theMontyChunker.chunk_multitag(lemmatised_text)

    def lemmatise_tagged(self,tagged_text):
        return self.theMontyLemmatiser.lemmatise_tagged_sentence(tagged_text)

                                   
    def extract_info(self,chunked_text):
    	return self.theMontyExtractor.extract_info(chunked_text,self.theMontyLemmatiser.lemmatise_tagged_sentence)
    
    
file=open(sys.argv[1],'r')
#file=open('prueba')
linea=file.readline()
m = MontyLingua()

lem=0

if sys.argv[2] == 'man':
	print 'Se calculara la distancia con Manhattan\n'
elif sys.argv[2] == 'can':
	print 'Se calculara la distancia con Canberra\n'	
elif sys.argv[2] == 'sqr':
	print 'Se calculara la distancia con SquareCord\n'		
elif sys.argv[2] == 'chi':
	print 'Se calculara la distancia de Squared ChiSquared\n'	
elif sys.argv[2] == 'ALL':
	print 'Se calcularan todas las distancias'
if len(sys.argv)==4:
	if sys.argv[3] == 'lem':
		p=MontyLemmatiser.MontyLemmatiser()
		lem=1
	
tot=[]
tot_w=[]
tot_f=[]

nav=[]
nav_w=[]
nav_f=[]

res=[]
res_w=[]
res_f=[]

inf=[]
inf_w=[]
inf_f=[]

dict_nav={}
dict_res={}
dict_inf={}

dict_tot={}

cant_inf=0.0
cant_nav=0.0
cant_res=0.0
cant_tot=0.0

punt=['.',',',':',';','-','?','&']
palabras=['a', 'about', 'above', 'above', 'across', 'after', 'afterwards', 'again', 'against', 'all', 'almost', 'alone', 'along', 'already', 'also','although','always','am','among', 'amongst', 'amoungst', 'amount',  'an', 'and', 'another', 'any','anyhow','anyone','anything','anyway', 'anywhere', 'are', 'around', 'as',  'at', 'back','be','became', 'because','become','becomes', 'becoming', 'been', 'before', 'beforehand', 'behind', 'being', 'below', 'beside', 'besides', 'between', 'beyond', 'bill', 'both', 'bottom','but', 'by', 'call', 'can', 'cannot', 'cant', 'co', 'con', 'could', 'couldnt', 'cry', 'de', 'describe', 'detail', 'do', 'done', 'down', 'due', 'during', 'each', 'eg', 'eight', 'either', 'eleven','else', 'elsewhere', 'empty', 'enough', 'etc', 'even', 'ever', 'every', 'everyone', 'everything', 'everywhere', 'except', 'few', 'fifteen', 'fify', 'fill', 'find', 'fire', 'first', 'five', 'for', 'former', 'formerly', 'forty', 'found', 'four', 'from', 'front', 'full', 'further', 'get', 'give', 'go', 'had', 'has', 'hasnt', 'have', 'he', 'hence', 'her', 'here', 'hereafter', 'hereby', 'herein', 'hereupon', 'hers', 'herself', 'him', 'himself', 'his', 'how', 'however', 'hundred', 'ie', 'if', 'in', 'inc', 'indeed', 'interest', 'into', 'is', 'it', 'its', 'itself', 'keep', 'last', 'latter', 'latterly', 'least', 'less', 'ltd', 'made', 'many', 'may', 'me', 'meanwhile', 'might', 'mill', 'mine', 'more', 'moreover', 'most', 'mostly', 'move', 'much', 'must', 'my', 'myself', 'name', 'namely', 'neither', 'never', 'nevertheless', 'next', 'nine', 'no', 'nobody', 'none', 'noone', 'nor', 'not', 'nothing', 'now', 'nowhere', 'of', 'off', 'often', 'on', 'once', 'one', 'only', 'onto', 'or', 'other', 'others', 'otherwise', 'our', 'ours', 'ourselves', 'out', 'over', 'own','part', 'per', 'perhaps', 'please', 'put', 'rather', 're', 'same', 'see', 'seem', 'seemed', 'seeming', 'seems', 'serious', 'several', 'she', 'should', 'show', 'side', 'since', 'sincere', 'six', 'sixty', 'so', 'some', 'somehow', 'someone', 'something', 'sometime', 'sometimes', 'somewhere', 'still', 'such', 'system', 'take', 'ten', 'than', 'that', 'the', 'their', 'them', 'themselves', 'then', 'thence', 'there', 'thereafter', 'thereby', 'therefore', 'therein', 'thereupon', 'these', 'they', 'thickv', 'thin', 'third', 'this', 'those', 'though', 'three', 'through', 'throughout', 'thru', 'thus', 'to', 'together', 'too', 'top', 'toward', 'towards', 'twelve', 'twenty', 'two', 'un', 'under', 'until', 'up', 'upon', 'us', 'very', 'via', 'was', 'we', 'well', 'were', 'what', 'whatever', 'when', 'whence', 'whenever', 'where', 'whereafter', 'whereas', 'whereby', 'wherein', 'whereupon', 'wherever', 'whether', 'which', 'while', 'whither', 'who', 'whoever', 'whole', 'whom', 'whose', 'why', 'will', 'with', 'within', 'without', 'would', 'yet', 'you', 'your', 'yours', 'yourself', 'yourselves', 'the']

tmp = ""
while linea:
	sp=linea.split('\t')
	if lem==0:
		sentences = m.split_sentences(sp[1])
		tokenized = map(m.tokenize,sentences)
		#tokenized = nltk.word_tokenize(sp[1])
	else:
		tokenized = map(lambda the_tokenizer_str:p.lemmatise_word(the_tokenizer_str,),sp[1].split())
	for i in tokenized:
		tmp = tmp+" "+i
	if sp[0]=='NAV':
		cant_nav=cant_nav+1.0
		nav.append(tmp)
	elif sp[0]=='INF':
		cant_inf=cant_inf+1.0
		inf.append(tmp)
	elif sp[0]=='RES':
		cant_res=cant_res+1.0
		res.append(tmp)
	cant_tot=cant_tot+1.0
	tot.append(tmp)
	linea=file.readline()
	tmp = ""
####################################################################
print 'Generando diccionario de palabras totales\n'
for i in tot:
	a = i.split()
	for j in a:
		if j not in palabras:
			if j not in punt:
				tot_w.append(j)
tot_w.sort()

for i in tot_w:
	if i not in dict_tot:
		dict_tot[i]=0

for i in tot_w:
	dict_tot[i] = dict_tot[i]+1
#############################################################
print 'Generando diccionario de palabras RES'
print 'Generando vector por consultas RES\n'
vec_con_res=[]
dict_con={}
for j in sorted(dict_tot):
		dict_con[j]=0
for i in res:
	a = i.split()
	for j in a:
		if j not in palabras:
			if j not in punt:
				res_w.append(j)
				dict_con[j] += 1
	vec_con_res.append(sorted(list(dict_con.items())))
	for n in sorted(dict_con):
		dict_con[n]=0
res_w.sort()

for i in dict_tot:
	if i not in dict_res:
		dict_res[i]=0

for i in res_w:
	dict_res[i] = dict_res[i]+1
################################################################
print 'Generando diccionario de palabras NAV'
print 'Generando vector por consultas NAV\n'
vec_con_nav=[]
dict_con={}
for j in sorted(dict_tot):
		dict_con[j]=0
for i in nav:
	a = i.split()
	for j in a:
		if j not in palabras:
			if j not in punt:
				nav_w.append(j)
				dict_con[j] += 1
	vec_con_nav.append(sorted(list(dict_con.items())))
	for n in sorted(dict_con):
		dict_con[n]=0
nav_w.sort()

for i in dict_tot:
	if i not in dict_nav:
		dict_nav[i]=0

for i in nav_w:
	dict_nav[i] = dict_nav[i]+1
##################################################################
print 'Generando diccionario de palabras INF'
print 'Generando vector por consultas INF\n'
vec_con_inf=[]
dict_con={}
for j in sorted(dict_tot):
		dict_con[j]=0
for i in inf:
	a = i.split()
	for j in a:
		if j not in palabras:
			if j not in punt:
				inf_w.append(j)
				dict_con[j] += 1
	vec_con_inf.append(sorted(list(dict_con.items())))
	for n in sorted(dict_con):
		dict_con[n]=0
inf_w.sort()

for i in dict_tot:
	if i not in dict_inf:
		dict_inf[i]=0

for i in inf_w:
	dict_inf[i] = dict_inf[i]+1
##################################################

b = open("vector_inf.txt","w")


if sys.argv[2] == 'man' or sys.argv[2] == 'ALL':

	a=open("dist_man.txt","w")
	resultado=[]

	dist_i=0
	dist_n=0
	dist_r=0
	cant_i=0
	cant_n=0
	cant_r=0
	i=""

	print 'Calculando distancia querys INF'
	for k in vec_con_inf:
		for j in k:
			if j[1] != 0: i += j[0]+" "
			dist_i += abs(j[1]-float(dict_inf[j[0]]/cant_inf))
			dist_n += abs(j[1]-float(dict_nav[j[0]]/cant_nav))
			dist_r += abs(j[1]-float(dict_res[j[0]]/cant_res))
		if dist_i < dist_n and dist_i < dist_r:
			cant_i += 1
			mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
		elif dist_n < dist_i and dist_n < dist_r:
			cant_n += 1
			mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
		elif dist_r < dist_i and dist_r < dist_n:
			cant_r += 1
			mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n" 
		else:
			if dist_i==dist_n:
				seed=random.randint(1, 2)
				if seed==1:
					cant_i += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
				else:
					cant_n += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
			elif dist_i==dist_r:
				seed=random.randint(1, 2)
				if seed==1:
					cant_i += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
				else:
					cant_r += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n"
			elif dist_n==dist_r:
				seed=random.randint(1, 2)
				if seed==1:
					cant_n += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
				else:
					cant_r += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n"
			else:
				seed=random.randint(1, 3)
				if seed==1:
					cant_i += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
				elif seed==2:
					cant_n += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
				else:
					cant_r += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n"
		a.write(mm)
		dist_i=0
		dist_n=0
		dist_r=0
		i=""

	tmp_i=[]
	tmp_i.append(cant_i)
	tmp_i.append(cant_n)
	tmp_i.append(cant_r)
	resultado.append(tmp_i)
	del tmp_i
	cant_i=0
	cant_n=0
	cant_r=0

	print 'Calculando distancia querys NAV'
	for k in vec_con_nav:
		for j in k:
			if j[1] != 0: i += j[0]+" "
			dist_i += abs(j[1]-float(dict_inf[j[0]]/cant_inf))
			dist_n += abs(j[1]-float(dict_nav[j[0]]/cant_nav))
			dist_r += abs(j[1]-float(dict_res[j[0]]/cant_res))
		if dist_i < dist_n and dist_i < dist_r:
			cant_i += 1
			mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
		elif dist_n < dist_i and dist_n < dist_r:
			cant_n += 1
			mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
		elif dist_r < dist_i and dist_r < dist_n:
			cant_r += 1
			mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n" 
		else:
			if dist_i==dist_n:
				seed=random.randint(1, 2)
				if seed==1:
					cant_i += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
				else:
					cant_n += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
			elif dist_i==dist_r:
				seed=random.randint(1, 2)
				if seed==1:
					cant_i += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
				else:
					cant_r += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n"
			elif dist_n==dist_r:
				seed=random.randint(1, 2)
				if seed==1:
					cant_n += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
				else:
					cant_r += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n"
			else:
				seed=random.randint(1, 3)
				if seed==1:
					cant_i += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
				elif seed==2:
					cant_n += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
				else:
					cant_r += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n"
		a.write(mm)
		dist_i=0
		dist_n=0
		dist_r=0
		i=""

	tmp_i=[]
	tmp_i.append(cant_i)
	tmp_i.append(cant_n)
	tmp_i.append(cant_r)
	resultado.append(tmp_i)
	del tmp_i
	cant_i=0
	cant_n=0
	cant_r=0

	print 'Calculando distancia querys RES'
	for k in vec_con_res:
		for j in k:
			if j[1] != 0: i += j[0]+" "
			dist_i += abs(j[1]-float(dict_inf[j[0]]/cant_inf))
			dist_n += abs(j[1]-float(dict_nav[j[0]]/cant_nav))
			dist_r += abs(j[1]-float(dict_res[j[0]]/cant_res))
		if dist_i < dist_n and dist_i < dist_r:
			cant_i += 1
			mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
		elif dist_n < dist_i and dist_n < dist_r:
			cant_n += 1
			mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
		elif dist_r < dist_i and dist_r < dist_n:
			cant_r += 1
			mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n" 
		else:
			if dist_i==dist_n:
				seed=random.randint(1, 2)
				if seed==1:
					cant_i += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
				else:
					cant_n += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
			elif dist_i==dist_r:
				seed=random.randint(1, 2)
				if seed==1:
					cant_i += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
				else:
					cant_r += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n"
			elif dist_n==dist_r:
				seed=random.randint(1, 2)
				if seed==1:
					cant_n += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
				else:
					cant_r += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n"
			else:
				seed=random.randint(1, 3)
				if seed==1:
					cant_i += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
				elif seed==2:
					cant_n += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
				else:
					cant_r += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n"
		a.write(mm)
		dist_i=0
		dist_n=0
		dist_r=0	
		i=""

	a.close()

	tmp_i=[]
	tmp_i.append(cant_i)
	tmp_i.append(cant_n)
	tmp_i.append(cant_r)
	resultado.append(tmp_i)
	del tmp_i
	cant_i=0
	cant_n=0
	cant_r=0

	print '\nDistancia Manhattan\n'
	for i in resultado:
		print i
	print ''
	#print dict_inf
	acc_m=((resultado[0][0]+resultado[1][1]+resultado[2][2])/(cant_inf+cant_nav+cant_res))
	try:
		rec_i_m=resultado[0][0]/float(resultado[0][0]+resultado[0][1]+resultado[0][2])
	except ZeroDivisionError:
		rec_i_m=0
	try:
		rec_n_m=resultado[1][1]/float(resultado[1][0]+resultado[1][1]+resultado[1][2])
	except ZeroDivisionError:
		rec_n_m=0
	try:
		rec_r_m=resultado[2][2]/float(resultado[2][0]+resultado[2][1]+resultado[2][2])
	except ZeroDivisionError:
		rec_r_m=0
	try:
		pre_i_m=resultado[0][0]/float(resultado[0][0]+resultado[1][0]+resultado[2][0])
	except ZeroDivisionError:
		pre_i_m=0
	try:
		pre_n_m=resultado[1][1]/float(resultado[0][1]+resultado[1][1]+resultado[2][1])
	except ZeroDivisionError:
		pre_n_m=0
	try:
		pre_r_m=resultado[2][2]/float(resultado[0][2]+resultado[1][2]+resultado[2][2])
	except ZeroDivisionError:
		pre_r_m=0	

	print 'Accuracy: \n\t{}%'.format(((resultado[0][0]+resultado[1][1]+resultado[2][2])/(cant_inf+cant_nav+cant_res))*100.0)
	print 'Recall: \n\tINF {}%\n\tNAV {}%\n\tRES {}%'.format(rec_i_m*100.0,rec_n_m*100.0,rec_r_m*100.0)
	print 'Precision: \n\tINF {}%\n\tNAV {}%\n\tRES {}%'.format(pre_i_m*100.0,pre_n_m*100.0,pre_r_m*100.0)
	try:
		f_i=2*((pre_i_m*rec_i_m)/(pre_i_m+rec_i_m))
	except ZeroDivisionError:
		f_i=0
	try:
		f_n=2*((pre_n_m*rec_n_m)/(pre_n_m+rec_n_m))
	except ZeroDivisionError:
		f_n=0
	try:
		f_r=2*((pre_r_m*rec_r_m)/(pre_r_m+rec_r_m))
	except ZeroDivisionError:
		f_r=0
	print 'F1 Score: \n\tINF {}\n\tNAV {}\n\tRES {}'.format(f_i,f_n,f_r)


if sys.argv[2] == 'can' or sys.argv[2] == 'ALL':

	a=open("dist_can.txt","w")
	resultado=[]

	dist_i=0
	dist_n=0
	dist_r=0
	cant_i=0
	cant_n=0
	cant_r=0
	i=""

	print 'Calculando distancia querys INF'
	for k in vec_con_inf:
		for j in k:
			if j[1] != 0: i += j[0]+" "
			try:
				dist_i += (abs(j[1]-float(dict_inf[j[0]]/cant_inf)))/(j[1]+float(dict_inf[j[0]]/cant_inf))
			except ZeroDivisionError:
				dist_i += 0
			try:
				dist_n += (abs(j[1]-float(dict_nav[j[0]]/cant_nav)))/(j[1]+float(dict_nav[j[0]]/cant_nav))
			except ZeroDivisionError:
				dist_n += 0
			try:
				dist_r += (abs(j[1]-float(dict_res[j[0]]/cant_res)))/(j[1]+float(dict_res[j[0]]/cant_res))
			except ZeroDivisionError:
				dist_r += 0
		if dist_i < dist_n and dist_i < dist_r:
			cant_i += 1
			mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
		elif dist_n < dist_i and dist_n < dist_r:
			cant_n += 1
			mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
		elif dist_r < dist_i and dist_r < dist_n:
			cant_r += 1
			mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n" 
		else:
			if dist_i==dist_n:
				seed=random.randint(1, 2)
				if seed==1:
					cant_i += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
				else:
					cant_n += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
			elif dist_i==dist_r:
				seed=random.randint(1, 2)
				if seed==1:
					cant_i += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
				else:
					cant_r += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n"
			elif dist_n==dist_r:
				seed=random.randint(1, 2)
				if seed==1:
					cant_n += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
				else:
					cant_r += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n"
			else:
				seed=random.randint(1, 3)
				if seed==1:
					cant_i += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
				elif seed==2:
					cant_n += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
				else:
					cant_r += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n"
		a.write(mm)
		dist_i=0
		dist_n=0
		dist_r=0
		i=""

	tmp_i=[]
	tmp_i.append(cant_i)
	tmp_i.append(cant_n)
	tmp_i.append(cant_r)
	resultado.append(tmp_i)
	del tmp_i
	cant_i=0
	cant_n=0
	cant_r=0

	print 'Calculando distancia querys NAV'
	for k in vec_con_nav:
		for j in k:
			if j[1] != 0: i += j[0]+" "
			try:
				dist_i += (abs(j[1]-float(dict_inf[j[0]]/cant_inf)))/(j[1]+float(dict_inf[j[0]]/cant_inf))
			except ZeroDivisionError:
				dist_i += 0
			try:
				dist_n += (abs(j[1]-float(dict_nav[j[0]]/cant_nav)))/(j[1]+float(dict_nav[j[0]]/cant_nav))
			except ZeroDivisionError:
				dist_n += 0
			try:
				dist_r += (abs(j[1]-float(dict_res[j[0]]/cant_res)))/(j[1]+float(dict_res[j[0]]/cant_res))
			except ZeroDivisionError:
				dist_r += 0
		if dist_i < dist_n and dist_i < dist_r:
			cant_i += 1
			mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
		elif dist_n < dist_i and dist_n < dist_r:
			cant_n += 1
			mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
		elif dist_r < dist_i and dist_r < dist_n:
			cant_r += 1
			mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n" 
		else:
			if dist_i==dist_n:
				seed=random.randint(1, 2)
				if seed==1:
					cant_i += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
				else:
					cant_n += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
			elif dist_i==dist_r:
				seed=random.randint(1, 2)
				if seed==1:
					cant_i += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
				else:
					cant_r += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n"
			elif dist_n==dist_r:
				seed=random.randint(1, 2)
				if seed==1:
					cant_n += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
				else:
					cant_r += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n"
			else:
				seed=random.randint(1, 3)
				if seed==1:
					cant_i += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
				elif seed==2:
					cant_n += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
				else:
					cant_r += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n"
		a.write(mm)
		dist_i=0
		dist_n=0
		dist_r=0
		i=""

	tmp_i=[]
	tmp_i.append(cant_i)
	tmp_i.append(cant_n)
	tmp_i.append(cant_r)
	resultado.append(tmp_i)
	del tmp_i
	cant_i=0
	cant_n=0
	cant_r=0

	print 'Calculando distancia querys RES'
	for k in vec_con_res:
		for j in k:
			if j[1] != 0: i += j[0]+" "
			try:
				dist_i += (abs(j[1]-float(dict_inf[j[0]]/cant_inf)))/(j[1]+float(dict_inf[j[0]]/cant_inf))
			except ZeroDivisionError:
				dist_i += 0
			try:
				dist_n += (abs(j[1]-float(dict_nav[j[0]]/cant_nav)))/(j[1]+float(dict_nav[j[0]]/cant_nav))
			except ZeroDivisionError:
				dist_n += 0
			try:
				dist_r += (abs(j[1]-float(dict_res[j[0]]/cant_res)))/(j[1]+float(dict_res[j[0]]/cant_res))
			except ZeroDivisionError:
				dist_r += 0
		if dist_i < dist_n and dist_i < dist_r:
			cant_i += 1
			mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
		elif dist_n < dist_i and dist_n < dist_r:
			cant_n += 1
			mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
		elif dist_r < dist_i and dist_r < dist_n:
			cant_r += 1
			mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n" 
		else:
			if dist_i==dist_n:
				seed=random.randint(1, 2)
				if seed==1:
					cant_i += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
				else:
					cant_n += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
			elif dist_i==dist_r:
				seed=random.randint(1, 2)
				if seed==1:
					cant_i += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
				else:
					cant_r += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n"
			elif dist_n==dist_r:
				seed=random.randint(1, 2)
				if seed==1:
					cant_n += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
				else:
					cant_r += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n"
			else:
				seed=random.randint(1, 3)
				if seed==1:
					cant_i += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
				elif seed==2:
					cant_n += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
				else:
					cant_r += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n"
		a.write(mm)
		dist_i=0
		dist_n=0
		dist_r=0	
		i=""

	a.close()

	tmp_i=[]
	tmp_i.append(cant_i)
	tmp_i.append(cant_n)
	tmp_i.append(cant_r)
	resultado.append(tmp_i)
	del tmp_i
	cant_i=0
	cant_n=0
	cant_r=0

	print '\nDistancia Canberra\n'
	for i in resultado:
		print i
	print ''
	#print dict_inf
	acc_m=((resultado[0][0]+resultado[1][1]+resultado[2][2])/(cant_inf+cant_nav+cant_res))
	try:
		rec_i_m=resultado[0][0]/float(resultado[0][0]+resultado[0][1]+resultado[0][2])
	except ZeroDivisionError:
		rec_i_m=0
	try:
		rec_n_m=resultado[1][1]/float(resultado[1][0]+resultado[1][1]+resultado[1][2])
	except ZeroDivisionError:
		rec_n_m=0
	try:
		rec_r_m=resultado[2][2]/float(resultado[2][0]+resultado[2][1]+resultado[2][2])
	except ZeroDivisionError:
		rec_r_m=0
	try:
		pre_i_m=resultado[0][0]/float(resultado[0][0]+resultado[1][0]+resultado[2][0])
	except ZeroDivisionError:
		pre_i_m=0
	try:
		pre_n_m=resultado[1][1]/float(resultado[0][1]+resultado[1][1]+resultado[2][1])
	except ZeroDivisionError:
		pre_n_m=0
	try:
		pre_r_m=resultado[2][2]/float(resultado[0][2]+resultado[1][2]+resultado[2][2])
	except ZeroDivisionError:
		pre_r_m=0	

	print 'Accuracy: \n\t{}%'.format(((resultado[0][0]+resultado[1][1]+resultado[2][2])/(cant_inf+cant_nav+cant_res))*100.0)
	print 'Recall: \n\tINF {}%\n\tNAV {}%\n\tRES {}%'.format(rec_i_m*100.0,rec_n_m*100.0,rec_r_m*100.0)
	print 'Precision: \n\tINF {}%\n\tNAV {}%\n\tRES {}%'.format(pre_i_m*100.0,pre_n_m*100.0,pre_r_m*100.0)
	try:
		f_i=2*((pre_i_m*rec_i_m)/(pre_i_m+rec_i_m))
	except ZeroDivisionError:
		f_i=0
	try:
		f_n=2*((pre_n_m*rec_n_m)/(pre_n_m+rec_n_m))
	except ZeroDivisionError:
		f_n=0
	try:
		f_r=2*((pre_r_m*rec_r_m)/(pre_r_m+rec_r_m))
	except ZeroDivisionError:
		f_r=0
	print 'F1 Score: \n\tINF {}\n\tNAV {}\n\tRES {}'.format(f_i,f_n,f_r)


if sys.argv[2] == 'sqr' or sys.argv[2] == 'ALL':

	a=open("dist_sqrcord.txt","w")
	resultado=[]

	dist_i=0
	dist_n=0
	dist_r=0
	cant_i=0
	cant_n=0
	cant_r=0
	i=""

	print 'Calculando distancia querys INF'
	for k in vec_con_inf:
		for j in k:
			if j[1] != 0: i += j[0]+" "
			dist_i += pow((math.sqrt(j[1])-math.sqrt(dict_inf[j[0]]/cant_inf)),2)
			dist_n += pow((math.sqrt(j[1])-math.sqrt(dict_nav[j[0]]/cant_nav)),2)
			dist_r += pow((math.sqrt(j[1])-math.sqrt(dict_nav[j[0]]/cant_res)),2)
		if dist_i < dist_n and dist_i < dist_r:
			cant_i += 1
			mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
		elif dist_n < dist_i and dist_n < dist_r:
			cant_n += 1
			mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
		elif dist_r < dist_i and dist_r < dist_n:
			cant_r += 1
			mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n" 
		else:
			if dist_i==dist_n:
				seed=random.randint(1, 2)
				if seed==1:
					cant_i += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
				else:
					cant_n += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
			elif dist_i==dist_r:
				seed=random.randint(1, 2)
				if seed==1:
					cant_i += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
				else:
					cant_r += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n"
			elif dist_n==dist_r:
				seed=random.randint(1, 2)
				if seed==1:
					cant_n += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
				else:
					cant_r += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n"
			else:
				seed=random.randint(1, 3)
				if seed==1:
					cant_i += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
				elif seed==2:
					cant_n += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
				else:
					cant_r += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n"
		a.write(mm)
		dist_i=0
		dist_n=0
		dist_r=0
		i=""

	tmp_i=[]
	tmp_i.append(cant_i)
	tmp_i.append(cant_n)
	tmp_i.append(cant_r)
	resultado.append(tmp_i)
	del tmp_i
	cant_i=0
	cant_n=0
	cant_r=0

	print 'Calculando distancia querys NAV'
	for k in vec_con_nav:
		for j in k:
			if j[1] != 0: i += j[0]+" "
			dist_i += pow((math.sqrt(j[1])-math.sqrt(dict_inf[j[0]]/cant_inf)),2)
			dist_n += pow((math.sqrt(j[1])-math.sqrt(dict_nav[j[0]]/cant_nav)),2)
			dist_r += pow((math.sqrt(j[1])-math.sqrt(dict_nav[j[0]]/cant_res)),2)
		if dist_i < dist_n and dist_i < dist_r:
			cant_i += 1
			mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
		elif dist_n < dist_i and dist_n < dist_r:
			cant_n += 1
			mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
		elif dist_r < dist_i and dist_r < dist_n:
			cant_r += 1
			mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n" 
		else:
			if dist_i==dist_n:
				seed=random.randint(1, 2)
				if seed==1:
					cant_i += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
				else:
					cant_n += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
			elif dist_i==dist_r:
				seed=random.randint(1, 2)
				if seed==1:
					cant_i += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
				else:
					cant_r += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n"
			elif dist_n==dist_r:
				seed=random.randint(1, 2)
				if seed==1:
					cant_n += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
				else:
					cant_r += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n"
			else:
				seed=random.randint(1, 3)
				if seed==1:
					cant_i += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
				elif seed==2:
					cant_n += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
				else:
					cant_r += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n"
		a.write(mm)
		dist_i=0
		dist_n=0
		dist_r=0
		i=""

	tmp_i=[]
	tmp_i.append(cant_i)
	tmp_i.append(cant_n)
	tmp_i.append(cant_r)
	resultado.append(tmp_i)
	del tmp_i
	cant_i=0
	cant_n=0
	cant_r=0

	print 'Calculando distancia querys RES'
	for k in vec_con_res:
		for j in k:
			if j[1] != 0: i += j[0]+" "
			dist_i += pow((math.sqrt(j[1])-math.sqrt(dict_inf[j[0]]/cant_inf)),2)
			dist_n += pow((math.sqrt(j[1])-math.sqrt(dict_nav[j[0]]/cant_nav)),2)
			dist_r += pow((math.sqrt(j[1])-math.sqrt(dict_nav[j[0]]/cant_res)),2)
		if dist_i < dist_n and dist_i < dist_r:
			cant_i += 1
			mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
		elif dist_n < dist_i and dist_n < dist_r:
			cant_n += 1
			mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
		elif dist_r < dist_i and dist_r < dist_n:
			cant_r += 1
			mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n" 
		else:
			if dist_i==dist_n:
				seed=random.randint(1, 2)
				if seed==1:
					cant_i += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
				else:
					cant_n += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
			elif dist_i==dist_r:
				seed=random.randint(1, 2)
				if seed==1:
					cant_i += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
				else:
					cant_r += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n"
			elif dist_n==dist_r:
				seed=random.randint(1, 2)
				if seed==1:
					cant_n += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
				else:
					cant_r += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n"
			else:
				seed=random.randint(1, 3)
				if seed==1:
					cant_i += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
				elif seed==2:
					cant_n += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
				else:
					cant_r += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n"
		a.write(mm)
		dist_i=0
		dist_n=0
		dist_r=0	
		i=""

	a.close()

	tmp_i=[]
	tmp_i.append(cant_i)
	tmp_i.append(cant_n)
	tmp_i.append(cant_r)
	resultado.append(tmp_i)
	del tmp_i
	cant_i=0
	cant_n=0
	cant_r=0

	print '\nDistancia SquareCord\n'
	for i in resultado:
		print i
	print ''
	#print dict_inf
	acc_m=((resultado[0][0]+resultado[1][1]+resultado[2][2])/(cant_inf+cant_nav+cant_res))
	try:
		rec_i_m=resultado[0][0]/float(resultado[0][0]+resultado[0][1]+resultado[0][2])
	except ZeroDivisionError:
		rec_i_m=0
	try:
		rec_n_m=resultado[1][1]/float(resultado[1][0]+resultado[1][1]+resultado[1][2])
	except ZeroDivisionError:
		rec_n_m=0
	try:
		rec_r_m=resultado[2][2]/float(resultado[2][0]+resultado[2][1]+resultado[2][2])
	except ZeroDivisionError:
		rec_r_m=0
	try:
		pre_i_m=resultado[0][0]/float(resultado[0][0]+resultado[1][0]+resultado[2][0])
	except ZeroDivisionError:
		pre_i_m=0
	try:
		pre_n_m=resultado[1][1]/float(resultado[0][1]+resultado[1][1]+resultado[2][1])
	except ZeroDivisionError:
		pre_n_m=0
	try:
		pre_r_m=resultado[2][2]/float(resultado[0][2]+resultado[1][2]+resultado[2][2])
	except ZeroDivisionError:
		pre_r_m=0	

	print 'Accuracy: \n\t{}%'.format(((resultado[0][0]+resultado[1][1]+resultado[2][2])/(cant_inf+cant_nav+cant_res))*100.0)
	print 'Recall: \n\tINF {}%\n\tNAV {}%\n\tRES {}%'.format(rec_i_m*100.0,rec_n_m*100.0,rec_r_m*100.0)
	print 'Precision: \n\tINF {}%\n\tNAV {}%\n\tRES {}%'.format(pre_i_m*100.0,pre_n_m*100.0,pre_r_m*100.0)
	try:
		f_i=2*((pre_i_m*rec_i_m)/(pre_i_m+rec_i_m))
	except ZeroDivisionError:
		f_i=0
	try:
		f_n=2*((pre_n_m*rec_n_m)/(pre_n_m+rec_n_m))
	except ZeroDivisionError:
		f_n=0
	try:
		f_r=2*((pre_r_m*rec_r_m)/(pre_r_m+rec_r_m))
	except ZeroDivisionError:
		f_r=0
	print 'F1 Score: \n\tINF {}\n\tNAV {}\n\tRES {}'.format(f_i,f_n,f_r)


if sys.argv[2] == 'chi' or sys.argv[2] == 'ALL':

	a=open("dist_chisqr.txt","w")
	resultado=[]

	dist_i=0
	dist_n=0
	dist_r=0
	cant_i=0
	cant_n=0
	cant_r=0
	i=""

	print 'Calculando distancia querys INF'
	for k in vec_con_inf:
		for j in k:
			if j[1] != 0: i += j[0]+" "
			try:
				dist_i += (pow(j[1]-float(dict_inf[j[0]]/cant_inf),2))/(j[1]+float(dict_inf[j[0]]/cant_inf))
			except ZeroDivisionError:
				dist_i += 0
			try:
				dist_n += (pow(j[1]-float(dict_nav[j[0]]/cant_nav),2))/(j[1]+float(dict_nav[j[0]]/cant_nav))
			except ZeroDivisionError:
				dist_n += 0
			try:
				dist_r += (pow(j[1]-float(dict_res[j[0]]/cant_res),2))/(j[1]+float(dict_res[j[0]]/cant_res))
			except ZeroDivisionError:
				dist_r += 0
		if dist_i < dist_n and dist_i < dist_r:
			cant_i += 1
			mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
		elif dist_n < dist_i and dist_n < dist_r:
			cant_n += 1
			mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
		elif dist_r < dist_i and dist_r < dist_n:
			cant_r += 1
			mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n" 
		else:
			if dist_i==dist_n:
				seed=random.randint(1, 2)
				if seed==1:
					cant_i += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
				else:
					cant_n += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
			elif dist_i==dist_r:
				seed=random.randint(1, 2)
				if seed==1:
					cant_i += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
				else:
					cant_r += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n"
			elif dist_n==dist_r:
				seed=random.randint(1, 2)
				if seed==1:
					cant_n += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
				else:
					cant_r += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n"
			else:
				seed=random.randint(1, 3)
				if seed==1:
					cant_i += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
				elif seed==2:
					cant_n += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
				else:
					cant_r += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n"
		a.write(mm)
		dist_i=0
		dist_n=0
		dist_r=0
		i=""

	tmp_i=[]
	tmp_i.append(cant_i)
	tmp_i.append(cant_n)
	tmp_i.append(cant_r)
	resultado.append(tmp_i)
	del tmp_i
	cant_i=0
	cant_n=0
	cant_r=0

	print 'Calculando distancia querys NAV'
	for k in vec_con_nav:
		for j in k:
			if j[1] != 0: i += j[0]+" "
			try:
				dist_i += (pow(j[1]-float(dict_inf[j[0]]/cant_inf),2))/(j[1]+float(dict_inf[j[0]]/cant_inf))
			except ZeroDivisionError:
				dist_i += 0
			try:
				dist_n += (pow(j[1]-float(dict_nav[j[0]]/cant_nav),2))/(j[1]+float(dict_nav[j[0]]/cant_nav))
			except ZeroDivisionError:
				dist_n += 0
			try:
				dist_r += (pow(j[1]-float(dict_res[j[0]]/cant_res),2))/(j[1]+float(dict_res[j[0]]/cant_res))
			except ZeroDivisionError:
				dist_r += 0
		if dist_i < dist_n and dist_i < dist_r:
			cant_i += 1
			mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
		elif dist_n < dist_i and dist_n < dist_r:
			cant_n += 1
			mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
		elif dist_r < dist_i and dist_r < dist_n:
			cant_r += 1
			mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n" 
		else:
			if dist_i==dist_n:
				seed=random.randint(1, 2)
				if seed==1:
					cant_i += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
				else:
					cant_n += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
			elif dist_i==dist_r:
				seed=random.randint(1, 2)
				if seed==1:
					cant_i += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
				else:
					cant_r += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n"
			elif dist_n==dist_r:
				seed=random.randint(1, 2)
				if seed==1:
					cant_n += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
				else:
					cant_r += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n"
			else:
				seed=random.randint(1, 3)
				if seed==1:
					cant_i += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
				elif seed==2:
					cant_n += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
				else:
					cant_r += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n"
		a.write(mm)
		dist_i=0
		dist_n=0
		dist_r=0
		i=""

	tmp_i=[]
	tmp_i.append(cant_i)
	tmp_i.append(cant_n)
	tmp_i.append(cant_r)
	resultado.append(tmp_i)
	del tmp_i
	cant_i=0
	cant_n=0
	cant_r=0

	print 'Calculando distancia querys RES'
	for k in vec_con_res:
		for j in k:
			if j[1] != 0: i += j[0]+" "
			try:
				dist_i += (pow(j[1]-float(dict_inf[j[0]]/cant_inf),2))/(j[1]+float(dict_inf[j[0]]/cant_inf))
			except ZeroDivisionError:
				dist_i += 0
			try:
				dist_n += (pow(j[1]-float(dict_nav[j[0]]/cant_nav),2))/(j[1]+float(dict_nav[j[0]]/cant_nav))
			except ZeroDivisionError:
				dist_n += 0
			try:
				dist_r += (pow(j[1]-float(dict_res[j[0]]/cant_res),2))/(j[1]+float(dict_res[j[0]]/cant_res))
			except ZeroDivisionError:
				dist_r += 0
		if dist_i < dist_n and dist_i < dist_r:
			cant_i += 1
			mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
		elif dist_n < dist_i and dist_n < dist_r:
			cant_n += 1
			mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
		elif dist_r < dist_i and dist_r < dist_n:
			cant_r += 1
			mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n" 
		else:
			if dist_i==dist_n:
				seed=random.randint(1, 2)
				if seed==1:
					cant_i += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
				else:
					cant_n += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
			elif dist_i==dist_r:
				seed=random.randint(1, 2)
				if seed==1:
					cant_i += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
				else:
					cant_r += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n"
			elif dist_n==dist_r:
				seed=random.randint(1, 2)
				if seed==1:
					cant_n += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
				else:
					cant_r += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n"
			else:
				seed=random.randint(1, 3)
				if seed==1:
					cant_i += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "INF\n"
				elif seed==2:
					cant_n += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "NAV\n"
				else:
					cant_r += 1
					mm=i + "\t" + str(dist_i) + "\t" + str(dist_n) + "\t" + str(dist_r) + "\t" + "RES\n"
		a.write(mm)
		dist_i=0
		dist_n=0
		dist_r=0	
		i=""

	a.close()

	tmp_i=[]
	tmp_i.append(cant_i)
	tmp_i.append(cant_n)
	tmp_i.append(cant_r)
	resultado.append(tmp_i)
	del tmp_i
	cant_i=0
	cant_n=0
	cant_r=0

	print '\nDistancia ChiSquared\n'
	for i in resultado:
		print i
	print ''
	#print dict_inf
	acc_m=((resultado[0][0]+resultado[1][1]+resultado[2][2])/(cant_inf+cant_nav+cant_res))
	try:
		rec_i_m=resultado[0][0]/float(resultado[0][0]+resultado[0][1]+resultado[0][2])
	except ZeroDivisionError:
		rec_i_m=0
	try:
		rec_n_m=resultado[1][1]/float(resultado[1][0]+resultado[1][1]+resultado[1][2])
	except ZeroDivisionError:
		rec_n_m=0
	try:
		rec_r_m=resultado[2][2]/float(resultado[2][0]+resultado[2][1]+resultado[2][2])
	except ZeroDivisionError:
		rec_r_m=0
	try:
		pre_i_m=resultado[0][0]/float(resultado[0][0]+resultado[1][0]+resultado[2][0])
	except ZeroDivisionError:
		pre_i_m=0
	try:
		pre_n_m=resultado[1][1]/float(resultado[0][1]+resultado[1][1]+resultado[2][1])
	except ZeroDivisionError:
		pre_n_m=0
	try:
		pre_r_m=resultado[2][2]/float(resultado[0][2]+resultado[1][2]+resultado[2][2])
	except ZeroDivisionError:
		pre_r_m=0	

	print 'Accuracy: \n\t{}%'.format(((resultado[0][0]+resultado[1][1]+resultado[2][2])/(cant_inf+cant_nav+cant_res))*100.0)
	print 'Recall: \n\tINF {}%\n\tNAV {}%\n\tRES {}%'.format(rec_i_m*100.0,rec_n_m*100.0,rec_r_m*100.0)
	print 'Precision: \n\tINF {}%\n\tNAV {}%\n\tRES {}%'.format(pre_i_m*100.0,pre_n_m*100.0,pre_r_m*100.0)
	try:
		f_i=2*((pre_i_m*rec_i_m)/(pre_i_m+rec_i_m))
	except ZeroDivisionError:
		f_i=0
	try:
		f_n=2*((pre_n_m*rec_n_m)/(pre_n_m+rec_n_m))
	except ZeroDivisionError:
		f_n=0
	try:
		f_r=2*((pre_r_m*rec_r_m)/(pre_r_m+rec_r_m))
	except ZeroDivisionError:
		f_r=0
	print 'F1 Score: \n\tINF {}\n\tNAV {}\n\tRES {}'.format(f_i,f_n,f_r)




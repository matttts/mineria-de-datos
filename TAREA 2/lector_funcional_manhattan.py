import MontyTokenizer, MontyTagger, MontyLemmatiser, MontyREChunker, MontyExtractor, MontyNLGenerator, sys

class MontyLingua:
    
    def __init__(self,trace_p=0):
        self.trace_p = trace_p
        self.theMontyTokenizer = MontyTokenizer.MontyTokenizer()
        self.theMontyLemmatiser = MontyLemmatiser.MontyLemmatiser()
        self.theMontyTagger = MontyTagger.MontyTagger(trace_p,self.theMontyLemmatiser)
        self.theMontyChunker = MontyREChunker.MontyREChunker()
        self.theMontyExtractor = MontyExtractor.MontyExtractor()
        self.theMontyNLGenerator = MontyNLGenerator.MontyNLGenerator()
        print '*********************************\n'

#
#  MAIN FUNCTIONS
#
    def generate_summary(self,vsoos):
        return self.theMontyNLGenerator.generate_summary(vsoos)
            
    def generate_sentence(self,vsoo,sentence_type='declaration',tense='past',s_dtnum=('',1),o1_dtnum=('',1),o2_dtnum=('',1),o3_dtnum=('',1)):
        return self.theMontyNLGenerator.generate_sentence(vsoo,sentence_type=sentence_type,tense=tense,s_dtnum=s_dtnum,o1_dtnum=o1_dtnum,o2_dtnum=o2_dtnum,o3_dtnum=o3_dtnum)
        
    def jist_predicates(self,text):
        infos = self.jist(text)
        svoos_list = []
        for info in infos:
            svoos = info['verb_arg_structures_concise']
            svoos_list.append(svoos)
        return svoos_list
    
    def jist(self,text):
        sentences = self.split_sentences(text)
        tokenized = map(self.tokenize,sentences)
        tagged = map(self.tag_tokenized,tokenized)
        chunked = map(self.chunk_tagged,tagged)
        extracted = map(self.extract_info,chunked)
        return extracted

    def pp_info(self,extracted_infos):
        for i in range(len(extracted_infos)):
            keys = extracted_infos[i].keys()
            keys.sort()
            print "\n\n   SENTENCE #%s DIGEST:\n"%str(i+1)
            for key in keys:
                print (key+": ").rjust(22) + str(extracted_infos[i][key])
    
    def split_paragraphs(self,text):
        return self.theMontyTokenizer.split_paragraphs(text)

    def split_sentences(self,text):
        return self.theMontyTokenizer.split_sentences(text)

    def tokenize(self,sentence,expand_contractions_p=1):
        return self.theMontyTokenizer.tokenize(sentence,expand_contractions_p)

    def tag_tokenized(self,tokenized_text):
        return self.theMontyTagger.tag_tokenized(tokenized_text)

    def strip_tags(self,tagged_or_chunked_text):
        toks = tagged_or_chunked_text.split()
        toks = filter(lambda x:'/' in x,toks)
        toks = map(lambda x:x.split('/')[0],toks)
        return ' '.join(toks)

    def parse_pred_arg(self,pp):
        pp.strip
        toks = pp.strip()[1:-1].split()
        args = ' '.join(toks)[1:-1].split('" "')
        return args
    
    def chunk_tagged(self,tagged_text):
        return self.theMontyChunker.Process(tagged_text)
    
    def chunk_lemmatised(self,lemmatised_text):
        return self.theMontyChunker.chunk_multitag(lemmatised_text)

    def lemmatise_tagged(self,tagged_text):
        return self.theMontyLemmatiser.lemmatise_tagged_sentence(tagged_text)

                                   
    def extract_info(self,chunked_text):
    	return self.theMontyExtractor.extract_info(chunked_text,self.theMontyLemmatiser.lemmatise_tagged_sentence)
    
    
file=open('split-00-matias-mella_tagged','r')
linea=file.readline()
m = MontyLingua()

tot=[]
tot_w=[]
tot_f=[]

nav=[]
nav_w=[]
nav_f=[]

res=[]
res_w=[]
res_f=[]

inf=[]
inf_w=[]
inf_f=[]

dict_nav={}
dict_res={}
dict_inf={}

dict_tot={}

punt=['.',',',':',';','-','?','&']
palabras=['a', 'about', 'above', 'above', 'across', 'after', 'afterwards', 'again', 'against', 'all', 'almost', 'alone', 'along', 'already', 'also','although','always','am','among', 'amongst', 'amoungst', 'amount',  'an', 'and', 'another', 'any','anyhow','anyone','anything','anyway', 'anywhere', 'are', 'around', 'as',  'at', 'back','be','became', 'because','become','becomes', 'becoming', 'been', 'before', 'beforehand', 'behind', 'being', 'below', 'beside', 'besides', 'between', 'beyond', 'bill', 'both', 'bottom','but', 'by', 'call', 'can', 'cannot', 'cant', 'co', 'con', 'could', 'couldnt', 'cry', 'de', 'describe', 'detail', 'do', 'done', 'down', 'due', 'during', 'each', 'eg', 'eight', 'either', 'eleven','else', 'elsewhere', 'empty', 'enough', 'etc', 'even', 'ever', 'every', 'everyone', 'everything', 'everywhere', 'except', 'few', 'fifteen', 'fify', 'fill', 'find', 'fire', 'first', 'five', 'for', 'former', 'formerly', 'forty', 'found', 'four', 'from', 'front', 'full', 'further', 'get', 'give', 'go', 'had', 'has', 'hasnt', 'have', 'he', 'hence', 'her', 'here', 'hereafter', 'hereby', 'herein', 'hereupon', 'hers', 'herself', 'him', 'himself', 'his', 'how', 'however', 'hundred', 'ie', 'if', 'in', 'inc', 'indeed', 'interest', 'into', 'is', 'it', 'its', 'itself', 'keep', 'last', 'latter', 'latterly', 'least', 'less', 'ltd', 'made', 'many', 'may', 'me', 'meanwhile', 'might', 'mill', 'mine', 'more', 'moreover', 'most', 'mostly', 'move', 'much', 'must', 'my', 'myself', 'name', 'namely', 'neither', 'never', 'nevertheless', 'next', 'nine', 'no', 'nobody', 'none', 'noone', 'nor', 'not', 'nothing', 'now', 'nowhere', 'of', 'off', 'often', 'on', 'once', 'one', 'only', 'onto', 'or', 'other', 'others', 'otherwise', 'our', 'ours', 'ourselves', 'out', 'over', 'own','part', 'per', 'perhaps', 'please', 'put', 'rather', 're', 'same', 'see', 'seem', 'seemed', 'seeming', 'seems', 'serious', 'several', 'she', 'should', 'show', 'side', 'since', 'sincere', 'six', 'sixty', 'so', 'some', 'somehow', 'someone', 'something', 'sometime', 'sometimes', 'somewhere', 'still', 'such', 'system', 'take', 'ten', 'than', 'that', 'the', 'their', 'them', 'themselves', 'then', 'thence', 'there', 'thereafter', 'thereby', 'therefore', 'therein', 'thereupon', 'these', 'they', 'thickv', 'thin', 'third', 'this', 'those', 'though', 'three', 'through', 'throughout', 'thru', 'thus', 'to', 'together', 'too', 'top', 'toward', 'towards', 'twelve', 'twenty', 'two', 'un', 'under', 'until', 'up', 'upon', 'us', 'very', 'via', 'was', 'we', 'well', 'were', 'what', 'whatever', 'when', 'whence', 'whenever', 'where', 'whereafter', 'whereas', 'whereby', 'wherein', 'whereupon', 'wherever', 'whether', 'which', 'while', 'whither', 'who', 'whoever', 'whole', 'whom', 'whose', 'why', 'will', 'with', 'within', 'without', 'would', 'yet', 'you', 'your', 'yours', 'yourself', 'yourselves', 'the']

tmp = ""
while linea:
	sp=linea.split('\t')
	sentences = m.split_sentences(sp[1])
        tokenized = map(m.tokenize,sentences)
	for i in tokenized:
		tmp = tmp+" "+i
	if sp[0]=='NAV':
		nav.append(tmp)
	elif sp[0]=='INF':
		inf.append(tmp)
	elif sp[0]=='RES':
		res.append(tmp)
	tot.append(tmp)
	linea=file.readline()
	tmp = ""
#############################################################
for i in res:
	a = i.split()
	for j in a:
		if j not in palabras:
			if j not in punt:
				res_w.append(j)
res_w.sort()

for i in res_w:
	if i not in dict_res:
		dict_res[i]=0

for i in res_w:
	dict_res[i] = dict_res[i]+1
################################################################
for i in nav:
	a = i.split()
	for j in a:
		if j not in palabras:
			if j not in punt:
				nav_w.append(j)
nav_w.sort()

for i in nav_w:
	if i not in dict_nav:
		dict_nav[i]=0

for i in nav_w:
	dict_nav[i] = dict_nav[i]+1
##################################################################
for i in inf:
	a = i.split()
	for j in a:
		if j not in palabras:
			if j not in punt:
				inf_w.append(j)
inf_w.sort()

for i in inf_w:
	if i not in dict_inf:
		dict_inf[i]=0

for i in inf_w:
	dict_inf[i] = dict_inf[i]+1
####################################################################
for i in tot:
	a = i.split()
	for j in a:
		if j not in palabras:
			if j not in punt:
				tot_w.append(j)
tot_w.sort()

for i in tot_w:
	if i not in dict_tot:
		dict_tot[i]=0

for i in tot_w:
	dict_tot[i] = dict_tot[i]+1
#############################################
vec_con_res=[]
dict_con={}
for i in res:
	a=i.split()
	for j in a:
		if j not in palabras:
			if j not in punt:
				if j not in dict_con:
					dict_con[j] = 0
	for j in a:
		if j not in palabras:
			if j not in punt:
				dict_con[j] = dict_con[j]+1
	vec_con_res.append(dict_con.items())
	dict_con.clear()
###############################################
vec_con_nav=[]
dict_con.clear()
for i in nav:
	a=i.split()
	for j in a:
		if j not in palabras:
			if j not in punt:
				if j not in dict_con:
					dict_con[j] = 0
	for j in a:
		if j not in palabras:
			if j not in punt:
				dict_con[j] = dict_con[j]+1
	vec_con_nav.append(dict_con.items())
	dict_con.clear()
#################################################
vec_con_inf=[]
dict_con.clear()
for i in inf:
	a=i.split()
	for j in a:
		if j not in palabras:
			if j not in punt:
				if j not in dict_con:
					dict_con[j] = 0
	for j in a:
		if j not in palabras:
			if j not in punt:
				dict_con[j] = dict_con[j]+1
	vec_con_inf.append(dict_con.items())
	dict_con.clear()
################################################
'''
a = open("q_res.txt","w")

for i in res:
	a.write(i)
	a.write("\n")
	
a.close()

a = open("q_res_dict.txt","w")

for i in sorted(dict_res):
	a.write(i)
	a.write("\t\t")
	a.write(str(dict_res[i]))
	a.write("\n")
	
a.close()
'''

for i in dict_res:
	dict_res[i]=dict_res[i]/388.0
for i in dict_nav:
	dict_nav[i]=dict_nav[i]/541.0
for i in dict_inf:
	dict_inf[i]=dict_inf[i]/2071.0

cen_res=0
for i in dict_res:
	cen_res=cen_res+dict_res[i]
cen_nav=0
for i in dict_nav:
	cen_nav=cen_nav+dict_nav[i]
cen_inf=0
for i in dict_inf:
	cen_inf=cen_inf+dict_inf[i]		
		
#print 'centroide inf {}\ncentroide res {}\ncentroide nav {}\n'.format(cen_inf,cen_res,cen_nav)

resultado=[]
cant_i=0
cant_n=0
cant_r=0

a = open("distancias.txt","w")
cont=0
v1=0
v2=0
dist_i=0
dist_n=0
dist_r=0
for i in vec_con_inf:
	for j in i:
		v2=j[1]
		if j[0] in dict_nav:
			v1=dict_nav[j[0]]
		if v1 > v2:
			dist_n=dist_n + (v1-v2)
		else:
			dist_n=dist_n + (v2-v1)
		v1=0
		if j[0] in dict_res:
			v1=dict_res[j[0]]
		if v1 > v2:
			dist_r=dist_r + (v1-v2)
		else:
			dist_r=dist_r + (v2-v1)
		v1=0
		if j[0] in dict_inf:
			v1=dict_inf[j[0]]
		if v1 > v2:
			dist_i=dist_i + (v1-v2)
		else:
			dist_i=dist_i + (v2-v1)
		v1=0
		v2=0
	a.write(inf[cont])
	a.write("\t\t")
	a.write(str(dist_i))
	a.write("\t\t")
	a.write(str(dist_n))
	a.write("\t\t")
	a.write(str(dist_r))
	a.write("\t\t")
	if dist_i < dist_n and dist_i < dist_r:
		cant_i=cant_i+1
		a.write("INF")
	elif dist_n < dist_i and dist_n < dist_r:
		cant_n=cant_n+1
		a.write("NAV")
	else:
		cant_r=cant_r+1
		a.write("RES")
	a.write("\n")
	cont=cont+1
	dist_i=0
	dist_n=0
	dist_r=0

tmp_i=[]
tmp_i.append(cant_i)
tmp_i.append(cant_n)
tmp_i.append(cant_r)
resultado.append(tmp_i)
del tmp_i
cant_i=0
cant_n=0
cant_r=0

a.write("####################################################################\n")
			
cont=0
v1=0
v2=0
dist_i=0
dist_n=0
dist_r=0
for i in vec_con_nav:
	for j in i:
		v2=j[1]
		if j[0] in dict_nav:
			v1=dict_nav[j[0]]
		if v1 > v2:
			dist_n=dist_n + (v1-v2)
		else:
			dist_n=dist_n + (v2-v1)
		v1=0
		if j[0] in dict_res:
			v1=dict_res[j[0]]
		if v1 > v2:
			dist_r=dist_r + (v1-v2)
		else:
			dist_r=dist_r + (v2-v1)
		v1=0
		if j[0] in dict_inf:
			v1=dict_inf[j[0]]
		if v1 > v2:
			dist_i=dist_i + (v1-v2)
		else:
			dist_i=dist_i + (v2-v1)
		v1=0
		v2=0
	a.write(nav[cont])
	a.write("\t\t")
	a.write(str(dist_i))
	a.write("\t\t")
	a.write(str(dist_n))
	a.write("\t\t")
	a.write(str(dist_r))
	a.write("\t\t")
	if dist_i < dist_n and dist_i < dist_r:
		cant_i=cant_i+1
		a.write("INF")
	elif dist_n < dist_i and dist_n < dist_r:
		cant_n=cant_n+1
		a.write("NAV")
	else:
		cant_r=cant_r+1
		a.write("RES")
	a.write("\n")
	cont=cont+1
	dist_i=0
	dist_n=0
	dist_r=0

tmp_i=[]
tmp_i.append(cant_i)
tmp_i.append(cant_n)
tmp_i.append(cant_r)
resultado.append(tmp_i)
del tmp_i
cant_i=0
cant_n=0
cant_r=0
	
a.write("####################################################################\n")
			
cont=0
v1=0
v2=0
dist_i=0
dist_n=0
dist_r=0
for i in vec_con_res:
	for j in i:
		v2=j[1]
		if j[0] in dict_nav:
			v1=dict_nav[j[0]]
		if v1 > v2:
			dist_n=dist_n + (v1-v2)
		else:
			dist_n=dist_n + (v2-v1)
		v1=0
		if j[0] in dict_res:
			v1=dict_res[j[0]]
		if v1 > v2:
			dist_r=dist_r + (v1-v2)
		else:
			dist_r=dist_r + (v2-v1)
		v1=0
		if j[0] in dict_inf:
			v1=dict_inf[j[0]]
		if v1 > v2:
			dist_i=dist_i + (v1-v2)
		else:
			dist_i=dist_i + (v2-v1)
		v1=0
		v2=0
	a.write(res[cont])
	a.write("\t\t")
	a.write(str(dist_i))
	a.write("\t\t")
	a.write(str(dist_n))
	a.write("\t\t")
	a.write(str(dist_r))
	a.write("\t\t")
	if dist_i < dist_n and dist_i < dist_r:
		cant_i=cant_i+1
		a.write("INF")
	elif dist_n < dist_i and dist_n < dist_r:
		cant_n=cant_n+1
		a.write("NAV")
	else:
		cant_r=cant_r+1
		a.write("RES")
	a.write("\n")
	cont=cont+1
	dist_i=0
	dist_n=0
	dist_r=0

a.close()

tmp_i=[]
tmp_i.append(cant_i)
tmp_i.append(cant_n)
tmp_i.append(cant_r)
resultado.append(tmp_i)
del tmp_i
cant_i=0
cant_n=0
cant_r=0
print 'Distancia Manhattan\n'
for i in resultado:
	print i
print ''
acc_m=((resultado[0][0]+resultado[1][1]+resultado[2][2])/3000.0)
rec_i_m=resultado[0][0]/float(resultado[0][0]+resultado[0][1]+resultado[0][2])
rec_n_m=resultado[1][1]/float(resultado[1][0]+resultado[1][1]+resultado[1][2])
rec_r_m=resultado[2][2]/float(resultado[2][0]+resultado[2][1]+resultado[2][2])
pre_i_m=resultado[0][0]/float(resultado[0][0]+resultado[1][0]+resultado[2][0])
pre_n_m=resultado[0][1]/float(resultado[0][1]+resultado[1][1]+resultado[2][1])
pre_r_m=resultado[0][2]/float(resultado[0][2]+resultado[1][2]+resultado[2][2])
'''print acc_m
print rec_i_m*100
print rec_n_m*100
print rec_r_m*100
print pre_i_m*100
print pre_n_m*100
print pre_r_m*100'''
print 'Accuracy: \n\t{}%'.format(((resultado[0][0]+resultado[1][1]+resultado[2][2])/3000.0)*100.0)
print 'Recall: \n\tINF {}%\n\tNAV {}%\n\tRES {}%'.format(resultado[0][0]/float(resultado[0][0]+resultado[0][1]+resultado[0][2])*100.0,resultado[1][1]/float(resultado[1][0]+resultado[1][1]+resultado[1][2])*100.0,resultado[2][2]/float(resultado[2][0]+resultado[2][1]+resultado[2][2])*100.0)
print 'Precision: \n\tINF {}%\n\tNAV {}%\n\tRES {}%'.format(resultado[0][0]/float(resultado[0][0]+resultado[1][0]+resultado[2][0])*100.0,resultado[0][1]/float(resultado[0][1]+resultado[1][1]+resultado[2][1])*100.0,resultado[0][2]/float(resultado[0][2]+resultado[1][2]+resultado[2][2])*100.0)
print 'F1 Score: \n\tINF {}\n\tNAV {}\n\tRES {}'.format(2*((pre_i_m*rec_i_m)/(pre_i_m+rec_i_m)),2*((pre_n_m*rec_n_m)/(pre_n_m+rec_n_m)),2*((pre_r_m*rec_r_m)/(pre_r_m+rec_r_m)))
